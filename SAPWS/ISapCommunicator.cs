﻿using System;
using System.Collections.Generic;
using System.ServiceModel;
using Interfaces;
using Interfaces.ProductionOrderWebRef;
using Interfaces.SalesOrderWebRef;
using RockdoorRepository;
using SAPWS.Helpers;

namespace SAPWS
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IService1" in both code and config file together.
    [ServiceContract]
    public interface ISapCommunicator
    {

        [OperationContract]
        void AddLinesToProductionOrder(Database db, int jobNo, List<BomItem> bomItems, string sessionId);

        [OperationContract]
        void ReplaceLinesOnProductionOrder(Database db, int docEntry, List<BomItem> bomItems, string sessionId);

        [OperationContract]
        string LoginForSessionId(Database db, string sapCompanyPassword);

        [OperationContract]
        void Logout(string sessionId);

        [OperationContract]
        bool RemoveStockFromProductionOrder(Database db, int jobNumber, List<BomItem> bomItems, string sessionId);

        [OperationContract]
        bool WriteOfProductionOrder(Database db, int jobNumber, string sessionId);

        [OperationContract]
        void WorkCompletion(Database db, List<int> jobNo, string sessionId);

        [OperationContract]
        bool StartRevisionProcess(Database db, int jobNo, string sessionId);

        [OperationContract]
        void CancelPo(Database db, int docEntry, string sessionId);

        [OperationContract]
        void CancelSo(Database db, int docEntry, string sessionId);

        [OperationContract]
        bool CreatePurchaseGoodsReceipt(List<EntryToBookIn> jobsToBookin, string sessionId);

        [OperationContract]
        int CreateDeliveryNote(int soDocEntry, string sessionId, Database db);

        [OperationContract]
        int DeliverAndInvoice(int soDocEntry, string sessionId, Database db);

        [OperationContract]
        DocumentNumbers DeliverAndInvoiceDocNums(int soDocEntry, string sessionId, Database db);

        [OperationContract]
        int CreateSalesInvoice(int delDocEntry, string sessionId, Database db);

        [OperationContract]
        int CreateDeliveryNoteLineSpecific(int soDocEntry, string sessionId, int lineNum, Database db);

        [OperationContract]
        DocumentNumbers CreateDeliveryAndInvoiceLineSpecific(int soDocEntry, string sessionId,
            int lineNum, Database db);

        [OperationContract]
        bool CreateGlassItem(string itemName, string itemDesc, double costPrice, double sellingPrice, int itemGrpCode, string sessionId, Database db);

        [OperationContract]
        bool UpdateGlassItem(string itemName, double costPrice, double sellingPrice, string sessionId, Database db);

        [OperationContract]
        bool CreateItem(string itemName, string itemDesc, double costPrice, double sellingPrice, int revision, ItemGroup itmGroup, string sessionId, Database db, int overrideItmGrp = 0);

        [OperationContract]
        int CreatePurchaseOrder(string acctNo, string comment, DateTime dueDate, List<poItem> poItems, string sessionId, Database db);

        [OperationContract]
        int CreatePurchaseOrderWithIntRef(string acctNo, string comment, DateTime dueDate, List<poItem> poItems, string sessionId, Database db, string intRef);

        [OperationContract]
        int SkinsForPanels(List<BomItem> skinItems, string rdPw, string panPw, bool live);

        [OperationContract]
        bool CancelFromSap(Database db, List<int> jobNo, string sessionId);

        [OperationContract]
        int CreateSO(Database db, Document sO, string sessionId);

        [OperationContract]
        bool UpdateSODeliveryDate(Database db, int jobNo, DateTime newDelDate, string sessionId);

        [OperationContract]
        int CreateProdOrder(Database db, ProductionOrder pO, string sessionId);

        [OperationContract]
        bool HasSageSOrder(Database db, int delNum);

        [OperationContract]
        int GetSODocNumFromDocEntry(Database db, int docEntry);

        [OperationContract]
        Document FetchSODocument(Database db, int docEntry, string sessionId);

        [OperationContract]
        int UpdateSO(Database db, Document sO, string sessionId);

        [OperationContract]
        ProductionOrder FetchPODocument(Database db, int docEntry, string sessionId);

        [OperationContract]
        int UpdatePO(Database db, ProductionOrder pO, string sessionId);

        [OperationContract]
        void ReleasePO(Database db, List<int> docEntry, string sessionId);

        [OperationContract]
        bool CreateRevisionItem(string itemName, string itemDesc, double costPrice, double sellingPrice, int revision, ItemGroup itmGroup, string sessionId, Database db, int overrideItmGrp = 0);

        [OperationContract]
        int CreateSOPan(Database db, Interfaces.PanelsSalesOrderWebRef.Document sO, string sessionId);

        [OperationContract]
        int UpdateSOPan(Database db, Interfaces.PanelsSalesOrderWebRef.Document sO, string sessionId);

        [OperationContract]
        int CreateProdPan(Database db, Interfaces.PanelProductionOrderWebRef.ProductionOrder pO, string sessionId);

        [OperationContract]
        int UpdateProdPan(Database db, Interfaces.PanelProductionOrderWebRef.ProductionOrder pO, string sessionId);

        [OperationContract]
        bool IsDirectToSage(Database db, int supplierId, int depot, DateTime? requiredDate);

        [OperationContract]
        Interfaces.PanelsSalesOrderWebRef.Document FetchSODocumentPan(Database db, int docEntry, string sessionId);

        [OperationContract]
        Interfaces.PanelProductionOrderWebRef.ProductionOrder FetchPODocumentPan(Database db, int docEntry,string sessionId);

        [OperationContract]
        void UpdateLineQtyOnProductionOrder(Database db, int docEntry, string itemCode, double newQty,
            string sessionId);
    }
 
}
