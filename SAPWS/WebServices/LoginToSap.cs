﻿using System;
using Interfaces.LoginWebRef;
using RockdoorRepository;
using SAPWS.Properties;
using Serilog;

namespace SAPWS.WebServices
{
    public class LoginToSap
    {
        readonly string _sapDbServer = Settings.Default.SapDBServer;
        readonly string _sapCompanyUser = Settings.Default.SapCompanyUser;
        readonly string _sapLicServer = Settings.Default.SapLicServer;

        //login to sap and get a session id out
        public string Login(Database db, string sapCompanyPassword)
        {
            Log.Information($"Login {db}");
            string loginSessionId = string.Empty;

            try
            {
                LoginService svcLogin = new LoginService();

                loginSessionId = svcLogin.Login(_sapDbServer, db.ToString(), LoginDatabaseType.dst_MSSQL2012, true, _sapCompanyUser,
                                     sapCompanyPassword, LoginLanguage.ln_English, true, _sapLicServer);
            }
            catch (Exception e)
            {
                Log.Error($"Login error {e.InnerException}");
            }
            Log.Information($"Login Session - {loginSessionId}");
            return loginSessionId;
        }

        //free up the sap API session
        public void LogOut(string sessionId)
        {
            try
            {
                var svcLogin = new LoginService();

                var msgHeader = new MsgHeader
                {
                    SessionID = sessionId
                };

                svcLogin.MsgHeaderValue = msgHeader;
                svcLogin.Logout();
            }
            catch (Exception e)
            {
                Log.Error($"Logout Session - {sessionId} - {e.InnerException}");
            }
            Log.Information($"Logout Session - {sessionId}");
        }

    }
}