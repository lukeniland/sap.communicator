﻿using Interfaces;

namespace SAPWS.Helpers
{
    public static class SapHelpers
    {
        public static int GetDocentry(IData data, int jobNo)
        {
            //get docentry for production order, will check here on the status of the PO, only planned or released PO's can be amended            
            return data.GetDocEntryForPo(jobNo);
        }

        public static int GetDocentryForSo(IData data, int jobNo)
        {
            return data.GetDocEntryForSo(jobNo);
        }

    }

    public class DocumentNumbers
    {
        public int DeliveryNum { get; set; }
        public int InvoiceNum { get; set; }
    }
}