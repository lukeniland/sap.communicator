using Interfaces;
using SAPWS.Services;

[assembly: WebActivator.PostApplicationStartMethod(typeof(SAPWS.App_Start.SimpleInjectorInitializer), "Initialize")]

namespace SAPWS.App_Start
{
    using System.Diagnostics;
    using System.Reflection;
    using Serilog;
    using SimpleInjector;
    using SimpleInjector.Integration.Wcf;
    using System.Web;

    public static class SimpleInjectorInitializer
    {
        /// <summary>Initialize the container and register it for the WCF ServiceHostFactory.</summary>
        public static void Initialize()
        {
            var dir = $@"{System.Web.Hosting.HostingEnvironment.ApplicationPhysicalPath}logs\" + "SapWS-{Date}.log";
            Log.Logger = new LoggerConfiguration()
                .MinimumLevel.Information()
                .WriteTo.RollingFile(dir)
                .CreateLogger();

            var container = new Container();
            container.Options.DefaultScopedLifestyle = new WcfOperationLifestyle();

            InitializeContainer(container);
            container.RegisterWcfServices(Assembly.GetExecutingAssembly());
            container.Verify();
            
            SimpleInjectorServiceHostFactory.SetContainer(container);


        }

        private static void InitializeContainer(Container container)
        {
            container.Register<IWriteOfJob, WriteOfJob>(Lifestyle.Scoped);
            container.Register<IWorkCompleteProdJobs, WorkCompleteProdJob>(Lifestyle.Scoped);
            container.Register<ISapSalesOrder, SapSalesOrderService>(Lifestyle.Scoped);
            container.Register<IReceiptFromProduction, ReceiptFromProduction>(Lifestyle.Scoped);
            
        }
    }
}