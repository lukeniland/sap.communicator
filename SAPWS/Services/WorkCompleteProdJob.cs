﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using Interfaces;
using Interfaces.ProductionOrderWebRef;
using SAPWS.Helpers;
using SAPWS.Properties;

namespace SAPWS.Services
{
    public class WorkCompleteProdJob : IWorkCompleteProdJobs
    {
        ProductionOrdersService _productionOrder;
                
        public bool CompleteProductionOrder(int salesOrderId, string sessionId, IData data)
        {
            CreateOrderHeader(sessionId);
            int docEntry = SapHelpers.GetDocentry(data, salesOrderId);
            var poOrder = GetByParams(data,docEntry);

            try
            {
                poOrder.ProductionOrderStatus = ProductionOrderProductionOrderStatus.boposClosed;
                poOrder.ProductionOrderStatusSpecified = true;
                _productionOrder.Update(poOrder);
            }
            catch (Exception)
            {
                return false;
            }           
            return true;
        }

        public void AddLineToPo(IData data, int jobNo, List<BomItem> itemsToAdd, string sessionId)
        {
            CreateOrderHeader(sessionId);
            int pOdocEntry = SapHelpers.GetDocentry(data, jobNo);
            ProductionOrder myPOrder = GetByParams(data, pOdocEntry);

            //get no of lines in the BOM
            int noLines = myPOrder.ProductionOrderLines.GetLength(0);

            //increase the size of the production order lines array
            var currItems = myPOrder.ProductionOrderLines;
            Array.Resize(ref currItems, noLines + itemsToAdd.Count);

            int i = 0;

            foreach (var item in itemsToAdd)
            {
                //create line that references the revision item
                ProductionOrderProductionOrderLine docLine1 = new ProductionOrderProductionOrderLine
                {
                    ItemNo = itemsToAdd[i].ItemCode,
                    BaseQuantity = itemsToAdd[i].Qty,
                    BaseQuantitySpecified = true
                };
                //add the line into the bom
                currItems[noLines + i] = docLine1;
                i++;
            }
            
            myPOrder.ProductionOrderLines = currItems;

            //expand potential subassembly items
            ExpandSubAssemblyItems(ref myPOrder, data);

            try
            {
                //update the order
                _productionOrder.Update(myPOrder);
            }
            catch (Exception ex)
            {
                data.InsertErrorToLog(ex.Message, "Add lines to PO", Convert.ToInt32(pOdocEntry));
            }

        }

        public void InactiveItemCheck(List<int> jobNos, IData data, string sessionId)
        {
            foreach (var job in jobNos)
            {
                //get all production orders
                var allPODocEntries = data.GetAllDocEntryForPo(job);
                if (!allPODocEntries.Any()) { continue; }
                foreach (var poDoc in allPODocEntries)
                {
                    //does po have inactive lines
                    var inactiveLines = data.GetAnyBomInactiveLines(poDoc);

                    if (inactiveLines.Any())
                    {
                        RemoveLinesFromPoByDocEntry(data, poDoc, inactiveLines, sessionId);
                        //fire off warning email
                        //string items = string.Join(",", inactiveLines);
                        //SendEmail.SendErrorEmail($"Inactive item(s) ({items}) removed from bom for work completion on {job}", $"Inactive Bom Item {job}", Settings.Default.smtp, Settings.Default.workcompleteEmail, "sapws@gap.uk.com");
                    }
                }
            }
        }

        private bool RemoveLinesFromPoByDocEntry(IData data, int docEntry, List<string> itemsToRemove, string sessionId)
        {
            CreateOrderHeader(sessionId);
            
            ProductionOrder myPOrder = GetByParams(data, docEntry);
            List<ProductionOrderProductionOrderLine> plist = myPOrder.ProductionOrderLines.ToList();
            plist = plist.Where(p => !itemsToRemove.Any(x => x.Contains(p.ItemNo))).ToList();

            try
            {
                myPOrder.ProductionOrderLines = new ProductionOrderProductionOrderLine[plist.Count];
                myPOrder.ProductionOrderLines = plist.ToArray();
                _productionOrder.Update(myPOrder);
                return true;
            }
            catch (Exception ex)
            {
                data.InsertErrorToLog(ex.Message, "Remove lines from PO", Convert.ToInt32(docEntry));
                return false;
            }
        }

        public void UpdateLinesOnPo(IData data, int docEntry, List<BomItem> newLines, string sessionId)
        {
            CreateOrderHeader(sessionId);
            
            ProductionOrder myPOrder = GetByParams(data, docEntry);

            //reset lines
            myPOrder.ProductionOrderLines = new ProductionOrderProductionOrderLine[newLines.Count];

            //increase the size of the production order lines array
            
            int i = 0;

            foreach (var item in newLines)
            {
                //create line that references the revision item
                ProductionOrderProductionOrderLine docLine1 = new ProductionOrderProductionOrderLine
                {
                    ItemNo = item.ItemCode,
                    BaseQuantity = item.Qty,
                    BaseQuantitySpecified = true
                };
                //add the line into the bom
                myPOrder.ProductionOrderLines[i] = docLine1;
                i++;
            }

            //expand potential subassembly items
            ExpandSubAssemblyItems(ref myPOrder, data);

            try
            {
                //update the order
                _productionOrder.Update(myPOrder);
            }
            catch (Exception ex)
            {
                data.InsertErrorToLog(ex.Message, "Replacing lines on PO", Convert.ToInt32(docEntry));
            }

        }

        public void UpdateLineQtyOnPo(IData data, int docEntry, string itemCode, double newQuantity, string sessionId)
        {
            CreateOrderHeader(sessionId);

            ProductionOrder myPOrder = GetByParams(data, docEntry);

            foreach (var pl in myPOrder.ProductionOrderLines)
            {
                if (pl.ItemNo == itemCode)
                {
                    pl.BaseQuantity = newQuantity;
                    pl.BaseQuantitySpecified = true;
                }
            }

            try
            {
                //update the order
                _productionOrder.Update(myPOrder);
            }
            catch (Exception ex)
            {
                data.InsertErrorToLog(ex.Message, "Updating line Qty on PO", Convert.ToInt32(docEntry));
            }

        }

        public bool UpdatePoStatus(IData data, List<int> docEntryList, bool close, string sessionId)
        {
            CreateOrderHeader(sessionId);
            foreach (var docEntry in docEntryList)
            {
                try
                {
                    var myPOrder = GetByParams(data, docEntry);

                    if (close)
                    {
                        myPOrder.ProductionOrderStatus = ProductionOrderProductionOrderStatus.boposClosed;
                        myPOrder.ProductionOrderStatusSpecified = true;
                    }
                    else
                    {
                        myPOrder.ProductionOrderStatus = ProductionOrderProductionOrderStatus.boposReleased;
                        myPOrder.ProductionOrderStatusSpecified = true;
                    }

                    _productionOrder.Update(myPOrder);
                }
                catch (Exception ex)
                {
                    data.InsertErrorToLog(ex.Message, "Update PO Status " + docEntry, Convert.ToInt32(docEntry));
                    return false;
                }
            }

            return true;
        }

        public bool CancelPo(int docEntry, string sessionId, IData data)
        {
            CreateOrderHeader(sessionId);

            var myOrderParams = new ProductionOrderParams
            {
                AbsoluteEntry = docEntry,
                AbsoluteEntrySpecified = true
            };

            try
            {
                _productionOrder.Cancel(myOrderParams);
            }
            catch (Exception ex)
            {
                data.InsertErrorToLog(ex.Message, "Cancel PO Error", Convert.ToInt32(docEntry));
                return false;
            }
            return true;
        }

        public bool RemoveLinesFromPo(IData data, int jobNo, List<BomItem> itemsToRemove, string sessionId)
        {
            CreateOrderHeader(sessionId);
            int pOdocEntry = SapHelpers.GetDocentry(data, jobNo);
            ProductionOrder myPOrder = GetByParams(data, pOdocEntry);
            List<ProductionOrderProductionOrderLine> plist = myPOrder.ProductionOrderLines.ToList();
            plist = plist.Where(p => !itemsToRemove.Any(x => x.ItemCode == p.ItemNo)).ToList();

            try
            {
                myPOrder.ProductionOrderLines = new ProductionOrderProductionOrderLine[plist.Count];
                myPOrder.ProductionOrderLines = plist.ToArray();
                _productionOrder.Update(myPOrder);
                return true;
            }
            catch (Exception ex)
            {
                data.InsertErrorToLog(ex.Message, "Remove lines from PO", Convert.ToInt32(pOdocEntry));
                return false;
            }
        }

        public int CreateRevProdOrder(IData data, long docEntry, string jobNo, int newRevNo, string revItem, string sessionId)
        {
            CreateOrderHeader(sessionId);
            //create revision production order
            ProductionOrder myPOrder = GetByParams(data, docEntry);
            ProductionOrderParams myPoParams = new ProductionOrderParams();

            myPOrder.ItemNo = revItem;
            myPOrder.U_AZU_RTYP = "R";
            myPOrder.U_AZU_RNO = newRevNo;
            myPOrder.U_AZU_RNOSpecified = true;
            myPOrder.U_AZU_CREV = newRevNo;
            myPOrder.U_AZU_CREVSpecified = true;
            myPOrder.CreationDate = DateTime.Now;
            myPOrder.CreationDateSpecified = true;
            myPOrder.DueDate = DateTime.Now;
            myPOrder.DueDateSpecified = true;
            myPOrder.ProductionOrderStatus = ProductionOrderProductionOrderStatus.boposPlanned;
            myPOrder.ProductionOrderStatusSpecified = true;

            //we need to pull bom lines from @azurd2 we may not necessarily want to base the bom of what is in SAP as it could be edited. @AZURD2 holds original bom.

            List<KeyValuePair<string,decimal>> bomItems = data.GetBOM(jobNo);

            //count lines
            myPOrder.ProductionOrderLines = new ProductionOrderProductionOrderLine[bomItems.Count];

            int i =0;
            foreach (var item in bomItems)
            {
                ProductionOrderProductionOrderLine nP = new ProductionOrderProductionOrderLine
                {
                    ItemNo = item.Key,
                    BaseQuantity = Convert.ToDouble(item.Value),
                    BaseQuantitySpecified = true
                };
                myPOrder.ProductionOrderLines[i] = nP;
                i++;
            }

            //expand potential subassembly items
            ExpandSubAssemblyItems(ref myPOrder, data);

            try
            {
                myPoParams = _productionOrder.Add(myPOrder);
            }
            catch (Exception ex)
            {
                data.InsertErrorToLog(ex.Message, "Create Revision Production Order", Convert.ToInt32(jobNo));
                return -1;
            }
            
            return Convert.ToInt32(myPoParams.AbsoluteEntry);
        }

        public bool UpdateRevoPOSAP(IData data, long docEntry, string jobNo, int newRevNo, string revItem, string sessionId)
        {
            CreateOrderHeader(sessionId);
            ProductionOrder myPOrder = GetByParams(data, docEntry);

            //change status to planned
            myPOrder.ProductionOrderStatus = ProductionOrderProductionOrderStatus.boposPlanned;
            myPOrder.ProductionOrderStatusSpecified = true;
            myPOrder.U_AZU_CREV = newRevNo;
            myPOrder.U_AZU_CREVSpecified = true;

            ProductionOrderProductionOrderLine newLine = new ProductionOrderProductionOrderLine()
            {
                ItemNo = revItem,
                BaseQuantity = 1,
                BaseQuantitySpecified = true
            };

            //get no of lines in the BOM
            int noLines = myPOrder.ProductionOrderLines.GetLength(0);

            //increase the size of the production order lines array
            var currItems = myPOrder.ProductionOrderLines;
            Array.Resize(ref currItems, noLines + 1);
            
            currItems[noLines] = newLine;

            myPOrder.ProductionOrderLines = currItems;

            //expand potential subassembly items
            ExpandSubAssemblyItems(ref myPOrder, data);

            try
            {
                _productionOrder.Update(myPOrder);
            }
            catch (Exception ex)
            {
                data.InsertErrorToLog(ex.Message, "Add revision line to Rev0 Production Order", Convert.ToInt32(jobNo));
                return false;
            }
            return true;
        }


        //get PO details from SAP
        private ProductionOrder GetByParams(IData data, long docEntry)
        {
            ProductionOrder myPOrder = null;

            try
            {
                //input params
                ProductionOrderParams myPOrderParms = new ProductionOrderParams
                {
                    AbsoluteEntry = docEntry,
                    AbsoluteEntrySpecified = true
                };

                //get the order
                myPOrder = _productionOrder.GetByParams(myPOrderParms);

            }
            catch (Exception ex)
            {
                data.InsertErrorToLog(ex.Message, "Get PO from SAP", Convert.ToInt32(docEntry));
            }

            return myPOrder;
        }

        public ProductionOrder GrabPO(string sessionId, long docEntry, IData data)
        {
            CreateOrderHeader(sessionId);
            return GetByParams(data, docEntry);
        }

        public void ReleaseProductionOrder(long docEntry, IData data, string sessionId)
        {
            throw new NotImplementedException();
        }

        private void CreateOrderHeader(string sessionId)
        {
            var msgHeader = new MsgHeader
            {
                SessionID = sessionId,
                ServiceName = MsgHeaderServiceName.ProductionOrdersService,
                ServiceNameSpecified = true
            };

            _productionOrder = new ProductionOrdersService
            {
                MsgHeaderValue = msgHeader
            };
        }

        public int CreateFullProductionOrder(ProductionOrder prodDoc, IData data, string sessionId)
        {
            //check if po exists and is not cancelled, then don't create another one 
            if (prodDoc.U_AZU_RNO == 0)
            {
                var podoc = data.GetDocEntryForPoIncludingComplete(Convert.ToInt32(prodDoc.U_AZU_JOB));
                if (podoc != 0)
                {
                    return podoc;
                }
            }

            CreateOrderHeader(sessionId);

            //expand potential subassembly items
            ExpandSubAssemblyItems(ref prodDoc, data);

            //remove in active items
            prodDoc.ProductionOrderLines = RemoveInActiveLines(prodDoc.ProductionOrderLines.ToList(), data, prodDoc.U_AZU_JOB).ToArray();

            //apply wastage
            prodDoc.ProductionOrderLines = ApplyWastage(prodDoc.ProductionOrderLines.ToList(), data).ToArray();

            ProductionOrderParams poParams;
            try
            {
                poParams = _productionOrder.Add(prodDoc);
            }
            catch (Exception ex)
            {
                data.InsertErrorToLog(ex.Message, "Create Full Production Order", 0);
                return 0;
            }
            return Convert.ToInt32(poParams.AbsoluteEntry);
        }

        private void ExpandSubAssemblyItems(ref ProductionOrder pDoc, IData data)
        {
            List<ProductionOrderProductionOrderLine> plinestoAdd = new List<ProductionOrderProductionOrderLine>();

            var originalLines = pDoc.ProductionOrderLines.ToList();

            //loop through items
            foreach (var pl in originalLines)
            {
                //check for assembly bom
                if (data.IsItemAssemblyBOM(pl.ItemNo))
                { 
                    //hold the item
                    var holdItem = pl.ItemNo;
                    //fetch items from assembly bom
                    var items = data.AssemblyBom(holdItem);
                    //add them to list
                    foreach (var item in items)
                    {
                        ProductionOrderProductionOrderLine dLine = new ProductionOrderProductionOrderLine
                        {
                            ItemNo = item.Item1,
                            BaseQuantity = Convert.ToDouble(item.Item2.Value),
                            BaseQuantitySpecified = true
                        };
                        plinestoAdd.Add(dLine);
                    }
                    //remove the original item
                    originalLines = originalLines.Where(x=>x.ItemNo != holdItem).ToList();
                }

            }
            
            if (plinestoAdd.Any())
            {
                originalLines.AddRange(plinestoAdd);
                pDoc.ProductionOrderLines = originalLines.ToArray();
            }
        }

        private List<ProductionOrderProductionOrderLine> ApplyWastage(List<ProductionOrderProductionOrderLine> lines, IData data)
        {
            List<ProductionOrderProductionOrderLine> plinestoAdd = new List<ProductionOrderProductionOrderLine>();
            //apply wastage according to item group
            var waste = data.GetWastage();

            //loop
            foreach (var pl in lines)
            {
                foreach (var ws in waste)
                {
                    var itmGrpCod = data.GetItmGrpCod(pl.ItemNo);
                    if (itmGrpCod == 0) continue;

                    if (ws.itmGrpCod == itmGrpCod)
                    {
                        //apply waste
                        pl.BaseQuantity = pl.BaseQuantity + (pl.BaseQuantity * (double) (ws.waste/100));
                    }
                }
                plinestoAdd.Add(pl);
            }
            
            return plinestoAdd;
        }

        private List<ProductionOrderProductionOrderLine> RemoveInActiveLines(List<ProductionOrderProductionOrderLine> lines, IData data, string jobNo)
        {
            List<ProductionOrderProductionOrderLine> plinestoAdd = new List<ProductionOrderProductionOrderLine>();

            List<string> plinesRemoved = new List<string>();

            //loop
            foreach (var pl in lines)
            {
                //is item active
                if (data.IsItemActive(pl.ItemNo))
                {
                    plinestoAdd.Add(pl);
                }
                else
                {
                    plinesRemoved.Add(pl.ItemNo);
                }
            }

            //if (plinesRemoved.Any())
            //{
            //    //fire off warning email
            //    string items = string.Join(",", plinesRemoved);
            //    SendEmail.SendErrorEmail($"Inactive item(s) ({items}) removed from bom on {jobNo}", $"Inactive Bom Item {jobNo}", Settings.Default.smtp, Settings.Default.warningEmail, "sapws@gap.uk.com");
            //}

            return plinestoAdd;
        }


        public int UpdateFullProductionOrder(ProductionOrder prodDoc, IData data, string sessionId)
        {
            CreateOrderHeader(sessionId);

            //expand potential subassembly items
            ExpandSubAssemblyItems(ref prodDoc, data);

            //remove in active items
            prodDoc.ProductionOrderLines = RemoveInActiveLines(prodDoc.ProductionOrderLines.ToList(), data, prodDoc.U_AZU_JOB).ToArray();

            try
            {
                _productionOrder.Update(prodDoc);
            }
            catch (Exception ex)
            {
                data.InsertErrorToLog(ex.Message, "Updating Production Order", 0);
                return 0;
            }
            return 1;
        }
    }
}