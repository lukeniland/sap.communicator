﻿using System;
using System.Collections.Generic;
using Interfaces;
using Interfaces.PanelProductionOrderWebRef;

namespace SAPWS.Services
{
    public class PanelsProductionOrder : IPanelsProductionOrder
    {
        ProductionOrdersService _productionOrder;

        private void CreateOrderHeader(string sessionId)
        {
            var msgHeader = new MsgHeader
            {
                SessionID = sessionId,
                ServiceName = MsgHeaderServiceName.ProductionOrdersService,
                ServiceNameSpecified = true
            };

            _productionOrder = new ProductionOrdersService
            {
                MsgHeaderValue = msgHeader
            };
        }
        public int CreateFullProductionOrderPan(ProductionOrder prodDoc, IData data, string sessionId)
        {
            //check if po exists and is not cancelled, then don't create another one
            var podoc = data.GetDocEntryForPoIncludingComplete(Convert.ToInt32(prodDoc.U_AZU_JOB));
            if (podoc != 0)
            {
                return podoc;
            }

            CreateOrderHeader(sessionId);

            ProductionOrderParams poParams;
            try
            {
                poParams = _productionOrder.Add(prodDoc);
            }
            catch (Exception ex)
            {
                data.InsertErrorToLog(ex.Message, "Create Full Production Order", 0);
                return 0;
            }
            return Convert.ToInt32(poParams.AbsoluteEntry);
        }

        public int UpdateFullProductionOrderPan(ProductionOrder prodDoc, IData data, string sessionId)
        {
            CreateOrderHeader(sessionId);

            try
            {
                _productionOrder.Update(prodDoc);
            }
            catch (Exception ex)
            {
                data.InsertErrorToLog(ex.Message, "Updating Production Order", 0);
                return 0;
            }
            return 1;
        }

        public bool UpdatePanPoStatus(IData data, List<int> docEntryList, bool close, string sessionId)
        {
            CreateOrderHeader(sessionId);
            foreach (var docEntry in docEntryList)
            {
                try
                {
                    var myPOrder = GetByParamsPan(data, docEntry);

                    if (close)
                    {
                        myPOrder.ProductionOrderStatus = ProductionOrderProductionOrderStatus.boposClosed;
                        myPOrder.ProductionOrderStatusSpecified = true;
                    }
                    else
                    {
                        myPOrder.ProductionOrderStatus = ProductionOrderProductionOrderStatus.boposReleased;
                        myPOrder.ProductionOrderStatusSpecified = true;
                    }

                    _productionOrder.Update(myPOrder);
                }
                catch (Exception ex)
                {
                    data.InsertErrorToLog(ex.Message, "Update PO Status " + docEntry, Convert.ToInt32(docEntry));
                    return false;
                }
            }

            return true;
        }

        private ProductionOrder GetByParamsPan(IData data, long docEntry)
        {
            ProductionOrder myPOrder = null;

            try
            {
                //input params
                ProductionOrderParams myPOrderParms = new ProductionOrderParams
                {
                    AbsoluteEntry = docEntry,
                    AbsoluteEntrySpecified = true
                };

                //get the order
                myPOrder = _productionOrder.GetByParams(myPOrderParms);

            }
            catch (Exception ex)
            {
                data.InsertErrorToLog(ex.Message, "Get Panels PO from SAP", Convert.ToInt32(docEntry));
            }

            return myPOrder;
        }

        public ProductionOrder GrabPOPan(string sessionId, long docEntry, IData data)
        {
            CreateOrderHeader(sessionId);
            return GetByParamsPan(data, docEntry);
        }
    }
}