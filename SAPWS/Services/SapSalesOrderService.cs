﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using Interfaces;
using Interfaces.SalesOrderWebRef;
using SAPWS.Helpers;

namespace SAPWS.Services
{
    public class SapSalesOrderService : ISapSalesOrder
    {
        private OrdersService _salesOrder;

        public bool RemoveProdLine(int prodJobId, string sessionId, IData data)
        {
            CreateOrderHeader(sessionId);
            int docentry = SapHelpers.GetDocentryForSo(data, prodJobId);
            Document salesOrder = GetByParams(docentry, data);
            List<DocumentDocumentLine> prodLines = salesOrder.DocumentLines.ToList();
            prodLines = prodLines.Where(p => p.ItemCode != prodJobId.ToString()).ToList();
            
            //if there are no line left on the order, cancel the whole lot
            if (prodLines.Count == 0)
            {
                return CancelSalesOrder(docentry);
            }
            else
            {
                try
                {
                    salesOrder.DocumentLines = new DocumentDocumentLine[prodLines.Count];
                    salesOrder.DocumentLines = prodLines.ToArray();
                    _salesOrder.Update(salesOrder);
                }
                catch (Exception)
                {
                    return false;
                }                
            }                                    

            return true;
        }

        private bool CancelSalesOrder(long docEntry)
        {
            try
            {
                DocumentParams orderParams = new DocumentParams
                {
                    DocEntry = docEntry,
                    DocEntrySpecified = true
                };
                _salesOrder.Cancel(orderParams);
            }
            catch (Exception)
            {
                return false;
            }
            
            return true;
        }

        public bool CancelSo(int docEntry, string sessionId, IData data)
        {
            //External cancel SalesOrder
            CreateOrderHeader(sessionId);

            var myOrderParams = new DocumentParams()
            {
                DocEntry = docEntry,
                DocEntrySpecified = true
            };

            try
            {
                _salesOrder.Cancel(myOrderParams);
            }
            catch (Exception ex)
            {
                data.InsertErrorToLog(ex.Message, "Cancel SO Error", Convert.ToInt32(docEntry));
                return false;
            }
            return true;
        }
        private Document GetByParams(long docEntry, IData data)
        {
            Document salesOrder = null;

            try
            {
                DocumentParams orderParams = new DocumentParams();
                orderParams.DocEntry = docEntry;
                orderParams.DocEntrySpecified = true;
                salesOrder = _salesOrder.GetByParams(orderParams);
            }
            catch (Exception ex)
            {
                data.InsertErrorToLog(ex.Message, "Get SO from SAP", Convert.ToInt32(docEntry));
            }

           
            
            return salesOrder;                        
        }

        public Document GrabSO(string sessionId, long docEntry, IData data)
        {
            CreateOrderHeader(sessionId);
            return GetByParams(docEntry, data);

        }

        private void CreateOrderHeader(string sessionId)
        {
            var msgHeader = new MsgHeader
            {
                SessionID = sessionId,
                ServiceName = MsgHeaderServiceName.OrdersService,
                ServiceNameSpecified = true
            };

            _salesOrder = new OrdersService
            {
                MsgHeaderValue = msgHeader
            };
        }

        public bool UpdateSalesOrder(int salesOrderId, string sessionId, IData data, string jobNo, int revNo)
        {
            CreateOrderHeader(sessionId);
            //update sales order to show we have a revision on it
            Document mySalesOrder = GetByParams(salesOrderId, data);

            //make sure we are working on the line with the jobnumber on it

            var jobLine =0;
            var i = 0;

            foreach (var line in mySalesOrder.DocumentLines)
            {
                if (line.U_AZU_JOB == jobNo)
                    jobLine = i;
                i++;
            }
            
            mySalesOrder.DocumentLines[jobLine].U_AZU_RNO = revNo;
            mySalesOrder.DocumentLines[jobLine].U_AZU_RNOSpecified = true;
            mySalesOrder.DocumentLines[jobLine].U_AZU_RTYP = "R";

            try
            {
                _salesOrder.Update(mySalesOrder);
            }
            catch (Exception ex)
            {
                data.InsertErrorToLog(ex.Message, "Update Sales Order Item", Convert.ToInt32(jobNo));
                return false;
            }  
           

            return true;
        }

        public int CreateFullSalesOrder(Document soDoc, IData data, string sessionId)
        {
            //do check on rockdoor sales order already existing return docentry if so
            if (soDoc.U_Window != 1)
            {
                foreach (var docLine in soDoc.DocumentLines)
                {
                    var jobno = docLine.U_AZU_JOB;
                    var res = data.GetDocEntryForSo(Convert.ToInt32(jobno));
                    if (res != 0)
                        return res;
                }
            }
            
            CreateOrderHeader(sessionId);

            DocumentParams soParams;
            try
            {
                soParams = _salesOrder.Add(soDoc);
            }
            catch (Exception ex)
            {
                data.InsertErrorToLog(ex.Message + _salesOrder.Url, "Create Full Sales Order", 0);
                return 0;
            }
            return Convert.ToInt32(soParams.DocEntry);

        }

        public int UpdateFullSalesOrder(Document soDoc, IData data, string sessionId)
        {
            CreateOrderHeader(sessionId);

            try
            {
                _salesOrder.Update(soDoc);
            }
            catch (Exception ex)
            {
                data.InsertErrorToLog(ex.Message + _salesOrder.Url, "Update Full Sales Order", 0);
                return 0;
            }
            return 1;

        }

        public bool UpdateDelDate(int jobNo, DateTime newDelDate, string sessionId, IData data)
        {
            CreateOrderHeader(sessionId);
            
            //update sales order to show we have a revision on it
            Document mySalesOrder = GetByParams(data.GetDocEntryForSo(jobNo), data);

            mySalesOrder.DocDueDate = newDelDate;
            mySalesOrder.DocDueDateSpecified = true;

            try
            {
                _salesOrder.Update(mySalesOrder);
            }
            catch (Exception ex)
            {
                data.InsertErrorToLog(ex.Message, "Update Sales Order Due Date", Convert.ToInt32(jobNo));
                return false;
            }
            
            return true;
        }


        public int CreateSalesOrder(List<BomItem> items, string cardCode, string comment, string sessionId, IData data, string gapAcctCode)
        {
            CreateOrderHeader(sessionId);

            DocumentParams soParams;
            int counter = 0;
            Document soOrder = new Document
            {
                DocDueDate = DateTime.Now,
                DocDueDateSpecified = true,
                DocDate = DateTime.Now,
                DocDateSpecified = true,
                CardCode = cardCode,
                Comments = comment,
                U_RDR_SELC = gapAcctCode,
                NumAtCard = comment,
                DocumentLines = new DocumentDocumentLine[items.Count]
            };
            
            foreach (var item in items)
            {
                DocumentDocumentLine soLine = new DocumentDocumentLine
                {
                    ItemCode = item.ItemCode,
                    Quantity = item.Qty,
                    QuantitySpecified = true,
                    UnitPrice = 0,
                    UnitPriceSpecified = true,
                    TaxCode = "01",
                    DiscountPercent = 0.0,
                    DiscountPercentSpecified = true,
                };
                soOrder.DocumentLines.SetValue(soLine, counter);
                counter++;
            }
            
            try
            {
                soParams = _salesOrder.Add(soOrder);
            }
            catch (Exception ex)
            {
                data.InsertErrorToLog(ex.Message, "Create Sales Order", 0);
                return 0;
            }
            return Convert.ToInt32(soParams.DocEntry);
        }

    }
}