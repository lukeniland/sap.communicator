﻿using System;
using System.Collections.Generic;
using Interfaces;
using Interfaces.PurchaseDelNoteRef;

namespace SAPWS.Services
{
    public class PurchaseDelivery : IPurchaseDelivery
    {
        private PurchaseDeliveryNotesService _deliveryNotesService;

        public bool PurchaseGoodsReceipt(List<EntryToBookIn> jobsToBookIn, string sessionId)
        {
            CreateOrderHeader(sessionId);

            var poDocument = new Document
            {
                DocumentLines = new DocumentDocumentLine[jobsToBookIn.Count]
            };

            var i = 0;

            foreach (var glassItem in jobsToBookIn)
            {
                var docLine1 = new DocumentDocumentLine
                {
                    TransactionType = DocumentDocumentLineTransactionType.botrntComplete,
                    TransactionTypeSpecified = true,
                    BaseType = 22,
                    BaseTypeSpecified = true,
                    BaseEntry = glassItem.DocEntry,
                    BaseEntrySpecified = true,
                    Quantity = 1,
                    QuantitySpecified = true,
                    LineNum = glassItem.LineNo,
                    LineNumSpecified = true,
                    BaseLine = glassItem.LineNo,
                    BaseLineSpecified = true
                };

                poDocument.DocumentLines.SetValue(docLine1, i);
                i++;
            }

            try
            {
                _deliveryNotesService.Add(poDocument);
                }
            catch (Exception)
            {
                return false;
            }

            return true;
            
        }

        public int PurchaseSkinsDelivery(int docEntry, List<BomItem> skinItems, string sessionId, IData data)
        {
            CreateOrderHeader(sessionId);
            DocumentParams pParams;

            var poDocument = new Document
            {
                DocumentLines = new DocumentDocumentLine[skinItems.Count]
            };

            var i = 0;

            foreach (var item in skinItems)
            {
                var docLine1 = new DocumentDocumentLine
                {
                    TransactionType = DocumentDocumentLineTransactionType.botrntComplete,
                    TransactionTypeSpecified = true,
                    BaseType = 22,
                    BaseTypeSpecified = true,
                    BaseEntry = docEntry,
                    BaseEntrySpecified = true,
                    ItemCode = item.ItemCode,
                    Quantity = item.Qty,
                    QuantitySpecified = true,
                    LineNum = i,
                    LineNumSpecified = true,
                    BaseLine = i,
                    BaseLineSpecified = true
                };

                poDocument.DocumentLines.SetValue(docLine1, i);
                i++;
            }

            try
            {
                pParams =_deliveryNotesService.Add(poDocument);
            }
            catch (Exception ex)
            {
                data.InsertErrorToLog(ex.Message, "Create Purchase Delivery", docEntry);
                return 0;
            }

            return Convert.ToInt32(pParams.DocEntry);
        }

        private void CreateOrderHeader(string sessionId)
        {
            var msgHeader = new MsgHeader
            {
                SessionID = sessionId,
                ServiceName = MsgHeaderServiceName.PurchaseDeliveryNotesService,
                ServiceNameSpecified = true
            };

            _deliveryNotesService = new PurchaseDeliveryNotesService
            {
                MsgHeaderValue = msgHeader
            };
        }
    }
}