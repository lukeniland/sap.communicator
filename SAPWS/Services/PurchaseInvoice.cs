﻿using System;
using System.Collections.Generic;
using Interfaces;
using Interfaces.PurchaseInvoiceWebRef;

namespace SAPWS.Services
{
    public class PurchaseInvoice : IPurchaseInvoice
    {
        private PurchaseInvoicesService  _purchaseInvoicesService;

        public int CreatePurchaseInvoice(int recDocEntry, List<BomItem> skinItems, string sessionId, IData data)
        {
            CreateOrderHeader(sessionId);

            DocumentParams invParams;

            var invDocument = new Document
            {
                DocumentLines = new DocumentDocumentLine[skinItems.Count]
            };

            var i = 0;

            foreach (var item in skinItems)
            {
                var docLine1 = new DocumentDocumentLine
                {
                    BaseEntry = recDocEntry,
                    BaseEntrySpecified = true,
                    BaseLine = i,
                    BaseLineSpecified = true,
                    BaseType = 20,
                    BaseTypeSpecified = true
                };

                invDocument.DocumentLines.SetValue(docLine1, i);
                i++;
            }

            try
            {
                invParams = _purchaseInvoicesService.Add(invDocument);
            }
            catch (Exception ex)
            {
                data.InsertErrorToLog(ex.Message, "Create Purchase Invoice", recDocEntry);
                return 0;
            }

            return Convert.ToInt32(invParams.DocEntry);
        }

        private void CreateOrderHeader(string sessionId)
        {
            var msgHeader = new MsgHeader
            {
                SessionID = sessionId,
                ServiceName = MsgHeaderServiceName.PurchaseInvoicesService,
                ServiceNameSpecified = true
            };

            _purchaseInvoicesService = new PurchaseInvoicesService()
            {
                MsgHeaderValue = msgHeader
            };
        }
    }
}