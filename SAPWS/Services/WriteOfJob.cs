﻿using Interfaces;

namespace SAPWS.Services
{
    public class WriteOfJob :IWriteOfJob
    {        
        //update the flag in sap so we know the item has been written off
        public bool SetWriteOfFlag(int jobNo, IData rockdoorData)
        {
            return rockdoorData.UpdateStage(jobNo, "U_ACC_WO");
        }

        //work complete the production order
        public bool WorkCompleteProdOrder(int jobNumber, string sessionId, IWorkCompleteProdJobs workCompProd,IData rockdoorData)
        {
            return workCompProd.CompleteProductionOrder(jobNumber, sessionId, rockdoorData);
        }

        //get rid of the prod order line from the sales order
        public bool RemoveProdOrderLine(int jobNmber,string sessionId, ISapSalesOrder sapSalesOrder, IData rockdoorData)
        {
            return sapSalesOrder.RemoveProdLine(jobNmber, sessionId,rockdoorData);            
        }

        //create a Recipt for a prodcution order
        public bool CreateReciptFromProduction(int jobNumber, string sessionId, IReceiptFromProduction prodReceipt, IData rockdoorData)
        {
            return prodReceipt.CreateReceipt(jobNumber, sessionId, rockdoorData);
        }
    }
}