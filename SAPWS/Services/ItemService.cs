﻿using System;
using Interfaces;
using Interfaces.ItemsWebRef;

namespace SAPWS.Services
{
    public class ItemService : ICreateItem
    {
        private ItemsService _itemService;
        public bool CreateItem(string revItem, int revNo, string jobNo, string cardCode, string sessionId, IData data)
        {
            //revision job items only!

            CreateOrderHeader(sessionId);
            
            string itemName;
            string rTyp = "";

            if (revNo == 0)
            {
                itemName = jobNo;
            }
            else
            {
                itemName = revItem;
                rTyp = "R";
            }

            try
            {
                Item myItem = new Item
                {
                    ItemCode = itemName,
                    ItemName = itemName,
                    U_AZU_JOB = jobNo,
                    U_AZU_RNO = revNo,
                    U_AZU_RNOSpecified = true,
                    U_AZU_RTYP = rTyp,
                    U_AZU_CARD = cardCode,
                    ItemsGroupCode = 100,
                    ItemsGroupCodeSpecified = true,
                    ItemType = ItemItemType.itItems,
                    ItemTypeSpecified = true,
                    InventoryItem = ItemInventoryItem.tYES,
                    InventoryItemSpecified = true,
                    SalesItem = ItemSalesItem.tYES,
                    SalesItemSpecified = true,
                    ItemWarehouseInfoCollection = new ItemItemWarehouseInfo[1]
                };

                myItem.ItemWarehouseInfoCollection[0] = new ItemItemWarehouseInfo
                {
                    Committed = 1,
                    CommittedSpecified = true
                };

                _itemService.Add(myItem);
                
            }
            catch (Exception ex)
            {
                data.InsertErrorToLog(ex.Message, "Create Revision Item", Convert.ToInt32(jobNo));
                return false;
            }

            return true;
        }

        public bool UpdateGlassItem(string itemCode, double itemCost, double sellingPrice, string sessionId, IData data)
        {
            try
            {
                Item myItem = GetByParams(itemCode, data);
                myItem.ItemPrices[0].Price = itemCost;
                myItem.ItemPrices[0].PriceSpecified = true;
                myItem.ItemPrices[1].Price = itemCost;
                myItem.ItemPrices[1].PriceSpecified = true;
                _itemService.Update(myItem);
            }
            catch (Exception ex)
            {
                data.InsertErrorToLog(ex.Message, "Error updating glass item" + itemCode, 0);
                return false;
            }
            return true;
        }

        public bool CreateNewItem(string itemName, string itemDesc, double costPrice, double sellingPrice, int revision, ItemGroup itmGroup, string sessionId, IData data, int overrideItmGrp = 0)
        {
            
            CreateOrderHeader(sessionId);

            var group = itmGroup == ItemGroup.Other && overrideItmGrp != 0 ? overrideItmGrp : Convert.ToInt32(itmGroup);

            try
            {
                Item myItem = new Item
                {
                    ItemsGroupCode = group,
                    ItemsGroupCodeSpecified = true,
                    ItemType = ItemItemType.itItems,
                    ItemTypeSpecified = true,
                    InventoryItem = ItemInventoryItem.tYES,
                    InventoryItemSpecified = true,
                    SalesItem = ItemSalesItem.tYES,
                    SalesItemSpecified = true,
                    AvgStdPrice = costPrice,
                    AvgStdPriceSpecified = true,
                    ItemWarehouseInfoCollection = new ItemItemWarehouseInfo[1],
                    ItemPrices = new ItemItemPrice[2]
                };

                if (revision == 0)
                {
                    //not a revision item
                    myItem.ItemCode = itemName;
                    myItem.ItemName = itemDesc;
                    myItem.U_AZU_JOB = itemName;
                    myItem.U_AZU_RNO = 0;
                    myItem.U_AZU_RNOSpecified = true;
                    myItem.U_AZU_RTYP = ""; 
                }
                else
                {
                    //its a revision, so create the item with a differnet id and rev details
                    myItem.ItemCode = itemName + "/" + revision + "R";
                    myItem.ItemName = itemDesc + "/" + revision + "R";
                    myItem.U_AZU_JOB = itemName;
                    myItem.U_AZU_RNO = revision;
                    myItem.U_AZU_RNOSpecified = true;
                    myItem.U_AZU_RTYP = "R";
                }

                myItem.ItemPrices[0] = new ItemItemPrice()
                {
                    PriceList = 2,
                    PriceListSpecified = true,
                    Price = costPrice,
                    PriceSpecified = true
                };

                myItem.ItemPrices[1] = new ItemItemPrice()
                {
                    PriceList = 1,
                    PriceListSpecified = true,
                    Price = sellingPrice,
                    PriceSpecified = true
                };

                myItem.ItemWarehouseInfoCollection[0] = new ItemItemWarehouseInfo
                {
                    Committed = 1,
                    CommittedSpecified = true
                };

                _itemService.Add(myItem);

            }
            catch (Exception ex)
            {
                data.InsertErrorToLog(ex.Message, "Create New Item", Convert.ToInt32(itemName));
                return false;
            }

            return true;
        }

        public bool CreateGlassItem(string itemCode, string itemName, double itemCost, double sellingPrice, int grpCode,
            string sessionId, IData data)
        {
            CreateOrderHeader(sessionId);

          try

        {
                Item myItem = new Item
                {
                    ItemCode = itemCode,
                    ItemName = itemName,
                    ItemsGroupCode = grpCode,
                    ItemsGroupCodeSpecified = true,
                    AvgStdPrice = itemCost,
                    AvgStdPriceSpecified = true,
                    ItemType = ItemItemType.itItems,
                    ItemTypeSpecified = true,
                    InventoryItem = ItemInventoryItem.tYES,
                    InventoryItemSpecified = true,
                    SalesItem = ItemSalesItem.tYES,
                    SalesItemSpecified = true,
                    ItemWarehouseInfoCollection = new ItemItemWarehouseInfo[1],
                    ItemPrices = new ItemItemPrice[2]
                };

                myItem.ItemPrices[0] = new ItemItemPrice()
                {
                    PriceList = 2,
                    PriceListSpecified = true,
                    Price = itemCost,
                    PriceSpecified = true
                };

                myItem.ItemPrices[1] = new ItemItemPrice()
                {
                    PriceList = 1,
                    PriceListSpecified = true,
                    Price = Convert.ToDouble(sellingPrice),
                    PriceSpecified = true
                };

                myItem.ItemWarehouseInfoCollection[0] = new ItemItemWarehouseInfo
                {
                    Committed = 0,
                    CommittedSpecified = true
                };

                _itemService.Add(myItem);

            }
            catch (Exception ex)
            {
                data.InsertErrorToLog(ex.Message, "Create Glass Item", Convert.ToInt32(itemCode));
                return false;
            }

            return true;
        }

        private void CreateOrderHeader(string sessionId)
        {
            var msgHeader = new MsgHeader
            {
                SessionID = sessionId,
                ServiceName = MsgHeaderServiceName.ItemsService,
                ServiceNameSpecified = true
            };

            _itemService = new ItemsService
            {
                MsgHeaderValue = msgHeader
            };
        }

         private Item GetByParams(string itemCode, IData data)
        {
            Item myItem = new Item();

            try
            {                
                ItemParams myItemParams = new ItemParams {ItemCode = itemCode};                
                myItem = _itemService.GetByParams(myItemParams);
            }
            catch (Exception ex)
            {
                data.InsertErrorToLog(ex.Message, "Error fetching item params " + itemCode, 0);
            }
            return myItem;
        }

    }
}