﻿using System;
using Interfaces;
using Interfaces.DeliveryWebRef;

namespace SAPWS.Services
{
    public class DeliveryNotes : IDeliveryNotes
    {
        private DeliveryNotesService _deliveryNotesService;

        public int CreateDeliveryNote(int soDocEntry, string sessionId, IData data)
        {
            //first check to see if we are already delivered
            //if so then return delivery note number
            var delChk = data.IsDelivered(soDocEntry);

            if (delChk.delivered)
                return delChk.delnum;
            
            CreateOrderHeader(sessionId);

            //get lines from sales order

            var lineNums = data.GetSOLineNums(soDocEntry);
            var nooflines = lineNums.Count;
            DocumentParams delNoteParams = new DocumentParams();

            var delDocument = new Document
            {
                DocumentLines = new DocumentDocumentLine[nooflines]
            };
            
            var i = 0;

            foreach (var line in lineNums)
            {
                var docLine1 = new DocumentDocumentLine
                {
                    BaseEntry = soDocEntry,
                    BaseEntrySpecified = true,
                    BaseLine = line,
                    BaseLineSpecified = true,
                    BaseType = 17,
                    BaseTypeSpecified = true
                };

                delDocument.DocumentLines.SetValue(docLine1, i);
                i++;
            }

            try
            {
                delNoteParams = _deliveryNotesService.Add(delDocument);
            }
            catch (Exception ex)
            {
                data.InsertErrorToLog(ex.Message, "Create Sales Delivery", soDocEntry);
                return 0;
            }

            return data.GetDelDocNum(Convert.ToInt32(delNoteParams.DocEntry));
        }

        public int CreateDeliveryNoteReturnDocEntry(int soDocEntry, string sessionId, IData data)
        {
            var delChk = data.IsDelivered(soDocEntry);

            if (delChk.delivered)
                return delChk.docentry;


            CreateOrderHeader(sessionId);

            //get lines from sales order

            var lineNums = data.GetSOLineNums(soDocEntry);
            var nooflines = lineNums.Count;
            DocumentParams delNoteParams = new DocumentParams();

            var delDocument = new Document
            {
                DocumentLines = new DocumentDocumentLine[nooflines]
            };

            var i = 0;

            foreach (var line in lineNums)
            {
                var docLine1 = new DocumentDocumentLine
                {
                    BaseEntry = soDocEntry,
                    BaseEntrySpecified = true,
                    BaseLine = line,
                    BaseLineSpecified = true,
                    BaseType = 17,
                    BaseTypeSpecified = true
                };

                delDocument.DocumentLines.SetValue(docLine1, i);
                i++;
            }

            try
            {
                delNoteParams = _deliveryNotesService.Add(delDocument);
            }
            catch (Exception ex)
            {
                data.InsertErrorToLog(ex.Message, "Create Sales Delivery", soDocEntry);
                return 0;
            }

            return Convert.ToInt32(delNoteParams.DocEntry);
        }

        public int CreateDeliveryNoteLineSpecific(int soDocEntry, int lineNum, string sessionId, IData data)
        {
            CreateOrderHeader(sessionId);

            DocumentParams delNoteParams = new DocumentParams();

            var delDocument = new Document
            {
                DocumentLines = new DocumentDocumentLine[1]
            };

            var i = 0;


                var docLine1 = new DocumentDocumentLine
                {
                    BaseEntry = soDocEntry,
                    BaseEntrySpecified = true,
                    BaseLine = lineNum,
                    BaseLineSpecified = true,
                    BaseType = 17,
                    BaseTypeSpecified = true
                };

                delDocument.DocumentLines.SetValue(docLine1, i);
        
            try
            {
                delNoteParams = _deliveryNotesService.Add(delDocument);
            }
            catch (Exception)
            {
                return 0;
            }

            return Convert.ToInt32(delNoteParams.DocEntry);
        }

        private void CreateOrderHeader(string sessionId)
        {
            var msgHeader = new MsgHeader
            {
                SessionID = sessionId,
                ServiceName = MsgHeaderServiceName.DeliveryNotesService,
                ServiceNameSpecified = true
            };

            _deliveryNotesService = new DeliveryNotesService()
            {
                MsgHeaderValue = msgHeader
            };
        }
    }
}