﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Web;

namespace SAPWS.Services
{
    public static class SendEmail
    {
        public static void SendErrorEmail(string message, string subject, string snmpServer, string addressList, string emailFrom)
        {
            SmtpClient smtpServer = new SmtpClient(snmpServer);
            MailMessage erroMsg = new MailMessage();
            erroMsg.From = new MailAddress(emailFrom);
            string[] emailAdd = addressList.Split(';');
            foreach (string thisAdd in emailAdd)
            {
                erroMsg.To.Add(new MailAddress(thisAdd));
            }
            erroMsg.Subject = subject;
            erroMsg.Body = message;
            try
            {
                smtpServer.Send(erroMsg);
            }
            catch (Exception ex)
            {
            }
            finally
            {
                smtpServer.Dispose();
            }
        }
    }
}
