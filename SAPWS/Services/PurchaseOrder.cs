﻿using System;
using System.Collections.Generic;
using Interfaces;
using Interfaces.PurchaseOrderWebRef;

namespace SAPWS.Services
{
    public class PurchaseOrder : IPurchaseOrder
    {
        private PurchaseOrdersService _poService;

        public int CreatePurchaseOrder(string acctNo, string comment, DateTime dueDate, List<poItem> poItems, string sessionId, IData data, string intRef = "")
        {
            CreateOrderHeader(sessionId);

            DocumentParams poParams;
            int counter = 0;
            Document poOrder = new Document
            {
                DocDueDate = dueDate,
                DocDueDateSpecified = true,
                DocDate = DateTime.Now,
                DocDateSpecified = true,
                CardCode = acctNo,
                Comments = comment,
                DocumentLines = new DocumentDocumentLine[poItems.Count],
                U_ROC_INRE = intRef
            };

            foreach (var item in poItems)
            {
                DocumentDocumentLine poLine = new DocumentDocumentLine
                {
                    ItemCode = item.ItemCode,
                    Quantity = 1,
                    QuantitySpecified = true,
                    UnitPrice = Convert.ToDouble(item.Price),
                    UnitPriceSpecified = true,
                    TaxCode = "01",
                    DiscountPercent = 0.0,
                    DiscountPercentSpecified = true,
                };
                poOrder.DocumentLines.SetValue(poLine, counter);
                counter++;
            }

            try
            {
                poParams = _poService.Add(poOrder);
            }
            catch (Exception ex)
            {
                data.InsertErrorToLog(ex.Message, "Create Purchase Order", 0);
                return 0;
            }
            return data.GetPODocNum(Convert.ToInt32(poParams.DocEntry));
        }

        public int CreatePurchaseOrderSkins(string acctNo, List<BomItem> skinItems, string sessionId, IData data, string comment)
        {
            CreateOrderHeader(sessionId);

            DocumentParams poParams;
            int counter = 0;
            Document poOrder = new Document
            {
                DocDueDate = DateTime.Now,
                DocDueDateSpecified = true,
                DocDate = DateTime.Now,
                DocDateSpecified = true,
                CardCode = acctNo,
                NumAtCard = comment,
                Comments = comment,
                DocumentLines = new DocumentDocumentLine[skinItems.Count]
            };

            foreach (var item in skinItems)
            {
                DocumentDocumentLine poLine = new DocumentDocumentLine
                {
                    ItemCode = item.ItemCode,
                    Quantity = item.Qty,
                    QuantitySpecified = true,
                    UnitPrice = 0,
                    UnitPriceSpecified = true,
                    TaxCode = "01",
                    DiscountPercent = 0.0,
                    DiscountPercentSpecified = true,
                };
                poOrder.DocumentLines.SetValue(poLine, counter);
                counter++;
            }

            try
            {
                poParams = _poService.Add(poOrder);
            }
            catch (Exception ex)
            {
                data.InsertErrorToLog(ex.Message, "Create Purchase Order", 0);
                return 0;
            }
            return Convert.ToInt32(poParams.DocEntry);
        }

        private void CreateOrderHeader(string sessionId)
        {
            var msgHeader = new MsgHeader
            {
                SessionID = sessionId,
                ServiceName = MsgHeaderServiceName.PurchaseOrdersService,
                ServiceNameSpecified = true
            };

            _poService = new PurchaseOrdersService()
            {
                MsgHeaderValue = msgHeader
            };


        }


    }
}