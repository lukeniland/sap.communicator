﻿using System;
using Interfaces;
using Interfaces.InvoiceWebRef;

namespace SAPWS.Services
{
    public class SalesInvoices : ISalesInvoices
    {
        private InvoicesService _invoicesService;

        public int CreateInvoiceNote(int delDocEntry, string sessionId, IData data)
        {
            var invChk = data.IsInvoiced(delDocEntry);

            if (invChk.invoiced)
                return invChk.docentry;

            CreateOrderHeader(sessionId);

            //get lines from delivery note
            var lineNums = data.GetDelLineNums(delDocEntry);
            var nooflines = lineNums.Count;
            DocumentParams invNoteParams = new DocumentParams();

            var delDocument = new Document
            {
                DocumentLines = new DocumentDocumentLine[nooflines]
            };

            var i = 0;

            foreach (var line in lineNums)
            {
                var docLine1 = new DocumentDocumentLine
                {
                    BaseEntry = delDocEntry,
                    BaseEntrySpecified = true,
                    BaseLine = line,
                    BaseLineSpecified = true,
                    BaseType = 15,
                    BaseTypeSpecified = true
                };

                delDocument.DocumentLines.SetValue(docLine1, i);
                i++;
            }

            try
            {
                invNoteParams = _invoicesService.Add(delDocument);
            }
            catch (Exception ex)
            {
                data.InsertErrorToLog(ex.Message, "Create Sales Invoice", delDocEntry);
                return 0;
            }

            return Convert.ToInt32(invNoteParams.DocEntry);
        }

        private void CreateOrderHeader(string sessionId)
        {
            var msgHeader = new MsgHeader
            {
                SessionID = sessionId,
                ServiceName = MsgHeaderServiceName.InvoicesService,
                ServiceNameSpecified = true
            };

            _invoicesService = new InvoicesService()
            {
                MsgHeaderValue = msgHeader
            };
        }

    }
}