﻿using System;
using System.Collections.Generic;
using Interfaces;
using Interfaces.GenEntryWebRef;
using SAPWS.Helpers;

namespace SAPWS.Services
{
    public class ReceiptFromProduction : IReceiptFromProduction
    {
        private InventoryGenEntryService _inventoryGen;

        public bool CreateReceipt(int jobNumber,string sessionId, IData data)
        {
            CreateOrderHeader(sessionId);
            int docEntry = SapHelpers.GetDocentry(data, jobNumber);
            var invGenDocument = new Document{DocumentLines = new DocumentDocumentLine[1]};

            var docLine1 = new DocumentDocumentLine
            {
                TransactionType = DocumentDocumentLineTransactionType.botrntComplete,
                TransactionTypeSpecified = true,
                BaseType = 202,
                BaseTypeSpecified = true,
                BaseEntry = docEntry,
                BaseEntrySpecified = true,
                Quantity = 1,
                QuantitySpecified = true
            };

            try
            {
                invGenDocument.DocumentLines.SetValue(docLine1, 0);
                _inventoryGen.Add(invGenDocument);
            }
            catch (Exception)
            {
                return false;
            }
            
            return true;
        }

        public bool CreateReceiptMultipleLines(List<int> docEntryList, string sessionId, IData data)
        {
            CreateOrderHeader(sessionId);
            try
            {
                var invGenDocument = new Document()
                {
                    DocumentLines = new DocumentDocumentLine[docEntryList.Count]
                };

                var i = 0;

                foreach (var docEntry in docEntryList)
                {
                    var docLine = new DocumentDocumentLine
                    {
                        TransactionType = DocumentDocumentLineTransactionType.botrntComplete,
                        TransactionTypeSpecified = true,
                        BaseType = 202,
                        BaseTypeSpecified = true,
                        BaseEntry = docEntry,
                        BaseEntrySpecified = true,
                        Quantity = 1,
                        QuantitySpecified = true
                    };
                    invGenDocument.DocumentLines[i] = docLine;
                    i++;
                }
                _inventoryGen.Add(invGenDocument);

            }
            catch (Exception ex)
            {
                data.InsertErrorToLog(ex.Message, "Create Receipt from production", docEntryList[0]);
                return false;
            }
            return true;
        }

        private void CreateOrderHeader(string sessionId)
        {
            var msgHeader = new MsgHeader
            {
                SessionID = sessionId,
                ServiceName = MsgHeaderServiceName.InventoryGenEntryService,
                ServiceNameSpecified = true
            };

            _inventoryGen = new InventoryGenEntryService
            {
                MsgHeaderValue = msgHeader
            };   


        }
    }
}