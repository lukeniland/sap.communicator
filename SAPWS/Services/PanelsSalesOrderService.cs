﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Interfaces;
using Interfaces.PanelsSalesOrderWebRef;

namespace SAPWS.Services
{
    public class PanelsSalesOrderService : IPanelsSalesOrder
    {
        private OrdersService _salesOrder;
        private void CreateOrderHeader(string sessionId)
        {
            var msgHeader = new MsgHeader
            {
                SessionID = sessionId,
                ServiceName = MsgHeaderServiceName.OrdersService,
                ServiceNameSpecified = true
            };

            _salesOrder = new OrdersService
            {
                MsgHeaderValue = msgHeader
            };
        }
      
        public int CreateFullSalesOrder(Document soDoc, IData data, string sessionId)
        {
            //do check on rockdoor sales order already existing return docentry if so
            foreach (var docLine in soDoc.DocumentLines)
                {
                    var jobno = docLine.U_AZU_JOB;
                    var res = data.GetDocEntryForSo(Convert.ToInt32(jobno));
                    if (res != 0)
                        return res;
                }
           
            CreateOrderHeader(sessionId);

            DocumentParams soParams;
            try
            {
                soParams = _salesOrder.Add(soDoc);
            }
            catch (Exception ex)
            {
                data.InsertErrorToLog(ex.Message + _salesOrder.Url, "Create Full Sales Order", 0);
                return 0;
            }
            return Convert.ToInt32(soParams.DocEntry);

        }

        private Document GetByParams(long docEntry, IData data)
        {
            Document salesOrder = null;

            try
            {
                DocumentParams orderParams = new DocumentParams();
                orderParams.DocEntry = docEntry;
                orderParams.DocEntrySpecified = true;
                salesOrder = _salesOrder.GetByParams(orderParams);
            }
            catch (Exception ex)
            {
                data.InsertErrorToLog(ex.Message, "Get SO from SAP", Convert.ToInt32(docEntry));
            }



            return salesOrder;
        }

        public Document GrabSOPan(string sessionId, long docEntry, IData data)
        {
            CreateOrderHeader(sessionId);
            return GetByParams(docEntry, data);

        }

        public int UpdateFullSalesOrder(Document soDoc, IData data, string sessionId)
        {
            CreateOrderHeader(sessionId);

            try
            {
                _salesOrder.Update(soDoc);
            }
            catch (Exception ex)
            {
                data.InsertErrorToLog(ex.Message + _salesOrder.Url, "Update Full Sales Order", 0);
                return 0;
            }
            return 1;

        }
        
    }
}