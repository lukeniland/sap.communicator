﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Web;
using Interfaces;
using Interfaces.ProductionOrderWebRef;
using Interfaces.SalesOrderWebRef;
using RockdoorRepository;
using RockdoorRepository.Repos;
using SAPWS.Properties;
using SAPWS.Services;
using SAPWS.WebServices;
using Sage;
using SAPWS.Helpers;
using Serilog;

namespace SAPWS
{
    public class SapCommunicator : ISapCommunicator
    {
        private readonly IWriteOfJob _writeOfJob;
        private readonly IWorkCompleteProdJobs _workCompleteProd;
        private readonly ISapSalesOrder _sapSalesOrder;
        private readonly IReceiptFromProduction _receiptFromProduction;

        public SapCommunicator(IWriteOfJob writeOfJob, IWorkCompleteProdJobs workCompleteProd,
            ISapSalesOrder sapSalesOrder, IReceiptFromProduction receiptFromProduction)
        {
            _writeOfJob = writeOfJob;
            _workCompleteProd = workCompleteProd;
            _sapSalesOrder = sapSalesOrder;
            _receiptFromProduction = receiptFromProduction;
        }

        public void AddLinesToProductionOrder(Database db, int jobNo, List<BomItem> bomItems, string sessionId)
        {
            IWorkCompleteProdJobs workComplete = new WorkCompleteProdJob();
            IData data = new DataRepo(db);
            workComplete.AddLineToPo(data, jobNo, bomItems, sessionId);
        }

        public bool CreatePurchaseGoodsReceipt(List<EntryToBookIn> jobsToBookin, string sessionId)
        {
            IPurchaseDelivery pDelv = new PurchaseDelivery();
            return pDelv.PurchaseGoodsReceipt(jobsToBookin, sessionId);
        }

        public int CreateDeliveryNote(int soDocEntry, string sessionId, Database db)
        {
            IDeliveryNotes delivery = new DeliveryNotes();
            IData data = new DataRepo(db);
            return delivery.CreateDeliveryNote(soDocEntry, sessionId, data);
        }

        public int DeliverAndInvoice(int soDocEntry, string sessionId, Database db)
        {
            IDeliveryNotes delivery = new DeliveryNotes();
            ISalesInvoices invoice = new SalesInvoices();
            IData data = new DataRepo(db);
            //do delivery
            var delDocEntry = delivery.CreateDeliveryNoteReturnDocEntry(soDocEntry, sessionId, data);
            //do invoice
            return invoice.CreateInvoiceNote(delDocEntry, sessionId, data);
        }

        public DocumentNumbers DeliverAndInvoiceDocNums(int soDocEntry, string sessionId,
            Database db)
        {
            IDeliveryNotes delivery = new DeliveryNotes();
            ISalesInvoices invoice = new SalesInvoices();
            IData data = new DataRepo(db);
            //do delivery
            var delDocEntry = delivery.CreateDeliveryNoteReturnDocEntry(soDocEntry, sessionId, data);
            //do invoice
            var invDocEntry = invoice.CreateInvoiceNote(delDocEntry, sessionId, data);
            DocumentNumbers result = new DocumentNumbers
            {
                DeliveryNum = data.GetDelDocNum(delDocEntry),
                InvoiceNum = data.GetInvDocNum(invDocEntry)
            };
            return result;
        }

        public int CreateSalesInvoice(int delDocEntry, string sessionId, Database db)
        {
            ISalesInvoices invoice = new SalesInvoices();
            IData data = new DataRepo(db);
            var invDoc = invoice.CreateInvoiceNote(delDocEntry, sessionId, data);
            return data.GetInvDocNum(invDoc);
        }

        public int CreateDeliveryNoteLineSpecific(int soDocEntry, string sessionId, int lineNum, Database db)
        {
            IDeliveryNotes delivery = new DeliveryNotes();
            IData data = new DataRepo(db);
            var deliveryDocEntry = delivery.CreateDeliveryNoteLineSpecific(soDocEntry, lineNum, sessionId, data);

            if (deliveryDocEntry == 0)
            {
                return 0;
            }
            var deliveryDocNum = data.GetDelDocNum(deliveryDocEntry);
            return deliveryDocNum;
        }

        public DocumentNumbers CreateDeliveryAndInvoiceLineSpecific(int soDocEntry, string sessionId, int lineNum, Database db)
        {
            IDeliveryNotes delivery = new DeliveryNotes();
            ISalesInvoices invoice = new SalesInvoices();
            IData data = new DataRepo(db);

            var deliveryDocEntry = delivery.CreateDeliveryNoteLineSpecific(soDocEntry, lineNum, sessionId, data);
            var invoiceDocEntry = invoice.CreateInvoiceNote(deliveryDocEntry, sessionId, data);

            DocumentNumbers result = new DocumentNumbers
            {
               DeliveryNum  = data.GetDelDocNum(deliveryDocEntry),
                InvoiceNum = data.GetInvDocNum(invoiceDocEntry)
            };

            return result;
        }

        public bool CreateGlassItem(string itemName, string itemDesc, double costPrice, double sellingPrice, int itemGrpCode, string sessionId, Database db)
        {
            ICreateItem createItem = new ItemService();
            IData data = new DataRepo(db);
            //check if item already exists first 
            bool result = true;
            if (!data.CheckItemExists(itemName))
            {
               result =  createItem.CreateGlassItem(itemName, itemDesc, costPrice, sellingPrice, itemGrpCode, sessionId, data);
            }
            return result;
        }

        public bool UpdateGlassItem(string itemName, double costPrice, double sellingPrice, string sessionId, Database db)
        {
            ICreateItem item = new ItemService();
            IData data = new DataRepo(db);
            //check if item already exists first 
            bool result = true;
            if (!data.CheckItemExists(itemName))
            {
                result = item.UpdateGlassItem(itemName, costPrice, sellingPrice, sessionId, data);
            }
            return result;
        }

        public bool CreateItem(string itemName, string itemDesc, double costPrice, double sellingPrice, int revision,
            ItemGroup itmGroup, string sessionId, Database db, int overrideItmGrp = 0)
        {
            ICreateItem createItem = new ItemService();
            IData data = new DataRepo(db);
            //check if item already exists first 
            bool result = true;
            if (!data.CheckItemExists(itemName))
            {
                result = createItem.CreateNewItem(itemName, itemDesc, costPrice, sellingPrice, revision, itmGroup, sessionId, data, overrideItmGrp);
            }
            return result;
        }

        public bool CreateRevisionItem(string itemName, string itemDesc, double costPrice, double sellingPrice, int revision,
            ItemGroup itmGroup, string sessionId, Database db, int overrideItmGrp = 0)
        {
            //revision only process, check for revision item first
            ICreateItem createItem = new ItemService();
            IData data = new DataRepo(db);
            //check if revision item already exists first 
            string revItem = $"{itemName}/{revision}R";
            bool result = true;
            if (!data.CheckItemExists(revItem))
            {
                result = createItem.CreateNewItem(itemName, itemDesc, costPrice, sellingPrice, revision, itmGroup, sessionId, data, overrideItmGrp);
            }
            return result;
        }

        public int CreatePurchaseOrder(string acctNo, string comment, DateTime dueDate, List<poItem> poItems, string sessionId, Database db)
        {
            IPurchaseOrder createOrder = new PurchaseOrder();
            IData data = new DataRepo(db);
            return createOrder.CreatePurchaseOrder(acctNo, comment, dueDate, poItems, sessionId, data);
        }

        public int CreatePurchaseOrderWithIntRef(string acctNo, string comment, DateTime dueDate, List<poItem> poItems, string sessionId, Database db, string intRef)
        {
            IPurchaseOrder createOrder = new PurchaseOrder();
            IData data = new DataRepo(db);
            return createOrder.CreatePurchaseOrder(acctNo, comment, dueDate, poItems, sessionId, data, intRef);
        }

        public int SkinsForPanels(List<BomItem> skinItems, string rdPw, string panPw, bool live)
        {
            Database rd;
            Database pan;

            if (live)
            {
                rd = Database.Rockdoorlive;
                pan = Database.PanelsLive;
            }
            else
            {
                rd = Database.Rockdoortest;
                pan = Database.Rockdoortest; //is no panels test company, create test stuff on rockdoor
            }
            
            var sessionIdRD = LoginForSessionId(rd, rdPw);
            var sessionIdPAN = LoginForSessionId(pan, panPw);
        
            //process
            IData dataRd = new DataRepo(rd);
            IData dataPan = new DataRepo(pan);

            //purchase order for skins on panels db V
            IPurchaseOrder createPOrder = new PurchaseOrder();
            int poDocEntry = createPOrder.CreatePurchaseOrderSkins(Settings.Default.RDaccount, skinItems, sessionIdPAN, dataPan, "Autogenerated purchase of skin from Rockdoor");

            //purchase order receipt for skins on panels db V
            IPurchaseDelivery createPDelivery = new PurchaseDelivery();
            int poRecDocEntry = createPDelivery.PurchaseSkinsDelivery(poDocEntry, skinItems, sessionIdPAN, dataPan);
            
            //purchase order invoice on panels db V
            IPurchaseInvoice createPInvoice = new PurchaseInvoice();
            int poInvDocEntry = createPInvoice.CreatePurchaseInvoice(poRecDocEntry, skinItems, sessionIdPAN, dataPan);


            //sales order for skins on rockdoor db V
            ISapSalesOrder createSalesOrder = new SapSalesOrderService();
            int soDocEntry = createSalesOrder.CreateSalesOrder(skinItems, Settings.Default.PANaccount, "Autogenerated Skin Sale to Panels", sessionIdRD, dataRd, Settings.Default.GapAcctCode);
            
            //delivery note for skins on rockdoor db V
            IDeliveryNotes createDeliveryNote = new DeliveryNotes();
            int delDocEntry = createDeliveryNote.CreateDeliveryNoteReturnDocEntry(soDocEntry, sessionIdRD, dataRd);
            
            //invoice for skins on rockdoor db V
            ISalesInvoices createSalesInvoice = new SalesInvoices();
            int invDocEntry = createSalesInvoice.CreateInvoiceNote(delDocEntry, sessionIdRD, dataRd);
            
            Logout(sessionIdRD);
            Logout(sessionIdPAN);

            return invDocEntry;
        }

        public void ReplaceLinesOnProductionOrder(Database db, int docEntry, List<BomItem> bomItems, string sessionId)
        {
            IWorkCompleteProdJobs workComplete = new WorkCompleteProdJob();
            IData data = new DataRepo(db);
            workComplete.UpdateLinesOnPo(data, docEntry, bomItems, sessionId);
        }

        public void UpdateLineQtyOnProductionOrder(Database db, int docEntry, string itemCode, double newQty, string sessionId)
        {
            IWorkCompleteProdJobs workComplete = new WorkCompleteProdJob();
            IData data = new DataRepo(db);
            workComplete.UpdateLineQtyOnPo(data, docEntry, itemCode, newQty, sessionId);
        }
        
        public bool RemoveStockFromProductionOrder(Database db, int jobNumber, List<BomItem> bomItems, string sessionId)
        {
            IWorkCompleteProdJobs workComplete = new WorkCompleteProdJob();
            IData data = new DataRepo(db);
            workComplete.RemoveLinesFromPo(data, jobNumber, bomItems, sessionId);
            return true;
        }

        public bool WriteOfProductionOrder(Database db, int jobNumber, string sessionId)
        {
            IData rockdoorData = new DataRepo(db);
            try
            {
                if (!_writeOfJob.SetWriteOfFlag(jobNumber, rockdoorData))
                {
                    return false;
                }
                if (!_writeOfJob.CreateReciptFromProduction(jobNumber, sessionId, _receiptFromProduction, rockdoorData))
                {
                    return false;
                }
                if (!_writeOfJob.WorkCompleteProdOrder(jobNumber, sessionId, _workCompleteProd, rockdoorData))
                {
                    return false;
                }
                if (!_writeOfJob.RemoveProdOrderLine(jobNumber, sessionId, _sapSalesOrder, rockdoorData))
                {
                    return false;
                }
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public void CancelPo(Database db, int docEntry, string sessionId)
        {
            IWorkCompleteProdJobs workComplete = new WorkCompleteProdJob();
            IData data = new DataRepo(db);
            workComplete.CancelPo(docEntry, sessionId, data);
        }

        public void CancelSo(Database db, int docEntry, string sessionId)
        {
            ISapSalesOrder salesOrder = new SapSalesOrderService();
            IData data = new DataRepo(db);
            salesOrder.CancelSo(docEntry, sessionId, data);
        }

        public bool CancelFromSap(Database db, List<int> jobNo, string sessionId)
        {
            IData data = new DataRepo(db);
            ISapSalesOrder salesOrder = new SapSalesOrderService();
            IWorkCompleteProdJobs workComplete = new WorkCompleteProdJob();

            List<CancelOrder> cOs = new List<CancelOrder>();

            //sales order may have lines for multiple jobnumbers
            //loop through jobs, assign sales order docentry and production order docentry, add number of lines
            foreach (var job in jobNo)
            {
                CancelOrder cO = new CancelOrder();
                cO.jobNo = job;
                cO.soDocEntry = data.GetDocEntryForSo(job);
                cO.poDocEntry = data.GetDocEntryForPo(job);
                cO.soLineNum = data.GetSoLineDetail(job.ToString());
                cO.soNoOfLines = data.GetSOLineNums(cO.soDocEntry).Count;
                cOs.Add(cO);
            }

            //pull out of this list grouped by soDocEntry, with count
            var grpSoDocs = cOs.GroupBy(x => x.soDocEntry).Select(n => new {soDoc = n.Key, soCount = n.Count()});
            
            //loop through the list and group by sales order docentry, check if number of lines equals number in group
            List<int> soToCancel = new List<int>();
            List<CancelOrder> removeLines = new List<CancelOrder>();
            foreach (var cO in cOs)
            {
                //create a list that can be cancelled straight off
                var soInfo = grpSoDocs.FirstOrDefault(x => x.soDoc == cO.soDocEntry);
                if (cO.soDocEntry != 0) //don't try to cancel what is already cancelled/removed
                {
                    if (cO.soNoOfLines == soInfo.soCount)
                    {
                        soToCancel.Add(cO.soDocEntry);
                    }
                    else
                    {
                        //otherwise add to list to remove lines from sales order
                        removeLines.Add(cO);
                    }
                }
            }
            
            //cancel so's
            foreach (var sentry in soToCancel)
            {
                salesOrder.CancelSo(sentry, sessionId, data);
            }

            //remove lines
            foreach (var line in removeLines)
            {
                salesOrder.RemoveProdLine(line.jobNo, sessionId, data);
            }

            //cancel production order
            foreach (var cO in cOs)
            {
                if (cO.poDocEntry != 0)
                {
                    workComplete.CancelPo(cO.poDocEntry, sessionId, data);
                }
            }
            
            return true;
        }

        public int CreateSO(Database db, Document sO, string sessionId)
        {
            IData rockdoorData = new DataRepo(db);
            ISapSalesOrder salesOrder = new SapSalesOrderService();
            return salesOrder.CreateFullSalesOrder(sO, rockdoorData, sessionId);
        }

        public bool UpdateSODeliveryDate(Database db, int jobNo, DateTime newDelDate, string sessionId)
        {
            IData rockdoorData = new DataRepo(db);
            ISapSalesOrder salesOrder = new SapSalesOrderService();
            return salesOrder.UpdateDelDate(jobNo, newDelDate, sessionId, rockdoorData);
        }

        public int CreateSOPan(Database db, Interfaces.PanelsSalesOrderWebRef.Document sO, string sessionId)
        {
            IData rockdoorData = new DataRepo(db);
            IPanelsSalesOrder salesOrder = new PanelsSalesOrderService();
            return salesOrder.CreateFullSalesOrder(sO, rockdoorData, sessionId);
        }

        public int UpdateSOPan(Database db, Interfaces.PanelsSalesOrderWebRef.Document sO, string sessionId)
        {
            IData rockdoorData = new DataRepo(db);
            IPanelsSalesOrder salesOrder = new PanelsSalesOrderService();
            return salesOrder.UpdateFullSalesOrder(sO, rockdoorData, sessionId);
        }

        public Interfaces.PanelProductionOrderWebRef.ProductionOrder FetchPODocumentPan(Database db, int docEntry, string sessionId)
        {
            IData rockdoorData = new DataRepo(db);
            IPanelsProductionOrder prodOrder = new PanelsProductionOrder();
            return prodOrder.GrabPOPan(sessionId, docEntry, rockdoorData);
        }

        public int CreateProdOrder(Database db, ProductionOrder pO, string sessionId)
        {
            IData rockdoorData = new DataRepo(db);
            IWorkCompleteProdJobs prodOrder = new WorkCompleteProdJob();
            return prodOrder.CreateFullProductionOrder(pO, rockdoorData, sessionId);
        }

        public int CreateProdPan(Database db, Interfaces.PanelProductionOrderWebRef.ProductionOrder pO, string sessionId)
        {
            IData rockdoorData = new DataRepo(db);
            IPanelsProductionOrder prodOrder = new PanelsProductionOrder();
            return prodOrder.CreateFullProductionOrderPan(pO, rockdoorData, sessionId);
        }

        public int UpdateProdPan(Database db, Interfaces.PanelProductionOrderWebRef.ProductionOrder pO, string sessionId)
        {
            IData rockdoorData = new DataRepo(db);
            IPanelsProductionOrder prodOrder = new PanelsProductionOrder();
            return prodOrder.UpdateFullProductionOrderPan(pO, rockdoorData, sessionId);
        }

        public bool IsDirectToSage(Database db, int supplierId, int depot, DateTime? requiredDate)
        {
            IData rockdoorData = new DataRepo(db);
            return rockdoorData.isDirect(depot, supplierId, requiredDate);
        }


        public bool HasSageSOrder(Database db, int delNum)
        {
            IData rockdoorData = new DataRepo(db);
            if (rockdoorData.GetSageSONumber(delNum) != null)
            {
                return true;
            }
            return false;
        }

        public int GetSODocNumFromDocEntry(Database db, int docEntry)
        {
            IData rockdoorData = new DataRepo(db);
            return rockdoorData.GetDocNumForSoWithDocEntry(docEntry);
        }

        public Document FetchSODocument(Database db, int docEntry, string sessionId)
        {
            IData rockdoorData = new DataRepo(db);
            ISapSalesOrder salesOrder = new SapSalesOrderService();
            return salesOrder.GrabSO(sessionId, docEntry, rockdoorData);
        }

        public Interfaces.PanelsSalesOrderWebRef.Document FetchSODocumentPan(Database db, int docEntry, string sessionId)
        {
            IData rockdoorData = new DataRepo(db);
            IPanelsSalesOrder salesOrder = new PanelsSalesOrderService();
            return salesOrder.GrabSOPan(sessionId, docEntry, rockdoorData);
        }

        public int UpdateSO(Database db, Document sO, string sessionId)
        {
            IData rockdoorData = new DataRepo(db);
            ISapSalesOrder salesOrder = new SapSalesOrderService();
            return salesOrder.UpdateFullSalesOrder(sO, rockdoorData, sessionId);
        }

        public ProductionOrder FetchPODocument(Database db, int docEntry, string sessionId)
        {
            IData rockdoorData = new DataRepo(db);
            IWorkCompleteProdJobs prodOrder = new WorkCompleteProdJob();
            return prodOrder.GrabPO(sessionId, docEntry, rockdoorData);
        }

        public int UpdatePO(Database db, ProductionOrder pO, string sessionId)
        {
            IData rockdoorData = new DataRepo(db);
            IWorkCompleteProdJobs prodOrder = new WorkCompleteProdJob();
            return prodOrder.UpdateFullProductionOrder(pO, rockdoorData, sessionId);
        }

        public void ReleasePO(Database db, List<int> docEntry, string sessionId)
        {
            IData rockdoorData = new DataRepo(db);
            IWorkCompleteProdJobs prodOrder = new WorkCompleteProdJob();
            IPanelsProductionOrder panelsProd = new PanelsProductionOrder();

            if (db == Database.PanelsLive || db == Database.PanelsTest)
            {
                panelsProd.UpdatePanPoStatus(rockdoorData, docEntry, false, sessionId);
            }
            else
            {
                prodOrder.UpdatePoStatus(rockdoorData, docEntry, false, sessionId);
            }
            
        }

        public void WorkCompletion(Database db, List<int> jobNo, string sessionId)
        {
            //full work completion process
            IData rockdoorData = new DataRepo(db);

            bool success = true;
            bool wcsuccess = true;
            //process

            //pull info for all jobNo's
            IData data = new DataRepo(db);
            List<PoInfo> poList = data.GetListOfPOs(jobNo);

            IWorkCompleteProdJobs workComplete = new WorkCompleteProdJob();

            //go through inactive item check
            workComplete.InactiveItemCheck(jobNo, rockdoorData, sessionId);

            //check if any of requested job numbers have status planned, if so change status to released
            List<int> posToRelease = poList.Where(x => x.Status == "P").Select(x => x.DocEntry).ToList();
            if (posToRelease.Any())
            {
                workComplete.UpdatePoStatus(rockdoorData, posToRelease, false, sessionId);
                poList = data.GetListOfPOs(jobNo); //refresh list now all PO's are R status
            }

            //do goods receipt on all jobs (only where we don't have a good receipt check first)
            List<int> posToReceipt = poList.Where(x => x.GoodsRecpt == 0).Select(x => x.DocEntry).ToList();
            if (success && posToReceipt.Any())
            {
                IReceiptFromProduction receipt = new ReceiptFromProduction();
                success = receipt.CreateReceiptMultipleLines(posToReceipt, sessionId, rockdoorData);
                poList = data.GetListOfPOs(jobNo);
            }

            //run through work completions only if receipt is done, do all revisions PO's first
            if (success && poList.Any(x => x.GoodsRecpt != 0))
            {
                //do work completion 
               wcsuccess =  workComplete.UpdatePoStatus(rockdoorData,
                    poList.Where(x => x.Status == "R").Select(x => x.DocEntry).ToList(), true, sessionId);
            }

            if (wcsuccess)
            {
                //update sage po's on applicable orders as the due date might have been changed by production since the PO was created...
                foreach (var job in jobNo)
                {
                    UpdateSagePOwithCurrentDocDueDate(db, job, data);
                }
            }

        }

        private bool UpdateSagePOwithCurrentDocDueDate(Database db, int jobNo, IData data)
        {
            //once we have details attempt to pull out sage po number and update the delivery date on it
            
            (OrderType, DateTime) type;

            if (db == Database.PanelsLive || db == Database.PanelsTest)
            {
                return true;
            }
            type = data.WhatIsThisOrder(jobNo.ToString());

            if (type.Item1 == OrderType.Window || type.Item1 == OrderType.RockdoorPortal)
            {
                //update sage using jobNo  
                //fetch the PO number from the portal db, just do a sql string for ease of use...


            var sagePonum = data.GetSagePOnumberFromPortal(jobNo.ToString());
            UpdatePurchaseOrder upO = new UpdatePurchaseOrder();
            upO.UpdateErpPurchaseOrder(sagePonum, type.Item2);

            }
            return true;
        }

        public bool StartRevisionProcess(Database db, int jobNo, string sessionId)
        {
            //in future may make this handle failures more carefully. As each stage is passed succesfully update a table. If a stage fails then on retry start again from the correct point.

            //check status 
            IData data = new DataRepo(db);
            JobStatus pinf = data.GetJobStatus(jobNo);

            ICreateItem item = new ItemService();
            ISapSalesOrder sales = new SapSalesOrderService();
            IWorkCompleteProdJobs prod = new WorkCompleteProdJob();

            var success = false;

            //do some checks on the status of the job
            //sometimes production order doesn't have current revision status
            //if so update production order current rev to match sales current revision

            int masterRno = pinf.AzuRno;
                //in theory azurd1 should always have the most uptodate record of the current revision number

            //if released or planned, ignore work complete orders...
            if (pinf.PoStatus == "R" || pinf.PoStatus == "P")
            {
                if (pinf.CurrentRevNo < masterRno)
                {
                    //check for item
                    var itemRs = data.GetItemRevs(jobNo);
                    string revItem = jobNo + "/" + masterRno + "R";

                    if (itemRs.All(x => x.RevNo != masterRno))
                    {
                        //add item if item doesn't already exist
                        item.CreateItem(revItem, masterRno, jobNo.ToString(), pinf.CardCode, sessionId,
                            data);
                        //add reference to item to PO
                        //update rev 0 bom/po with currentRevNo and item
                        prod.UpdateRevoPOSAP(data, pinf.PoDocEntry, jobNo.ToString(), masterRno, revItem, sessionId);
                    }
                    else
                    {
                        //item already exists, update PO with current revision
                        //check whether PO has the item in it
                        if (!data.CheckForRevItem(jobNo.ToString(), 0, revItem))
                        {
                            //if not add it rev 0 bom/po
                            prod.UpdateRevoPOSAP(data, pinf.PoDocEntry, jobNo.ToString(), masterRno, revItem,
                                sessionId);
                        }
                        else
                        {
                            //just update with current revision
                            data.UpdatePOwithCurrentRevision(pinf.PoDocEntry, masterRno);
                        }
                    }
                }

                if (pinf.SalesRevNo < masterRno)
                {
                    //update rdr1 with masterRno
                    data.UpdateRDR1withCurrentRevision(pinf.SoDocEntry, masterRno);
                }

                ///////MAIN PROCESS IF EVERYTHING IS OK\/\/\/\/\/\/\////////

                //increment revision number
                pinf.NewRevNo = masterRno + 1;

                //update @azurd1
                data.UpdateAzurd1(jobNo, pinf.NewRevNo);

                //update rockit
                if (pinf.JobSource == 0) //rockit job
                {
                    data.UpdateRockit(jobNo, pinf.NewRevNo);
                }


                string newRevItem = jobNo + "/" + pinf.NewRevNo + "R";

                //create revision item
                //check for item first
                var ritems = data.GetItemRevs(jobNo);
                
                if (ritems.All(x => x.ItemCode != newRevItem))
                {
                    success = item.CreateItem(newRevItem, pinf.NewRevNo, jobNo.ToString(), pinf.CardCode, sessionId,
                        data);
                }

                //update SO to have new rev and rtype
                if (success)
                {
                    success = sales.UpdateSalesOrder(pinf.SoDocEntry, sessionId, data, jobNo.ToString(),
                        pinf.NewRevNo);
                }

                //create new revision PO
                int newPOdocEntry = -1;
                if (success)
                {
                    newPOdocEntry = prod.CreateRevProdOrder(data, pinf.PoDocEntry, jobNo.ToString(), pinf.NewRevNo,
                        newRevItem, sessionId);
                }

                //update revision 0 BoM, add reference item
                if (success)
                {
                    prod.UpdateRevoPOSAP(data, pinf.PoDocEntry, jobNo.ToString(), pinf.NewRevNo, newRevItem, sessionId);
                }

                //release new revision PO
                if (newPOdocEntry != -1)
                {
                    //list of docentries
                    List<int> po = new List<int> {newPOdocEntry};
                    success = prod.UpdatePoStatus(data, po, false, sessionId);
                }
                else
                {
                    success = false;
                }
            }
            return success;
        }

        public string LoginForSessionId(Database db, string sapCompanyPassword)
        {
            string loginSessionId;

            try
            {
                LoginToSap login = new LoginToSap();

                loginSessionId = login.Login(db, sapCompanyPassword);
            }
            catch (Exception ex)
            {
                IData data = new DataRepo(db);
                data.InsertErrorToLog(ex.InnerException.ToString(), "Login for sessionId", 0);
                return "failed to get session ID";
            }

            if (loginSessionId.Length == 0)
            {
                IData data = new DataRepo(db);
                data.InsertErrorToLog("Failed to get sessionId/Login to SAP", "Login for sessionId", 0);
            }
            return loginSessionId;
        }

        public void Logout(string sessionId)
        {
            LoginToSap login = new LoginToSap();

            login.LogOut(sessionId);
        }
    }
}