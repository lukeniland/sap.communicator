﻿using System.Collections.Generic;
using Interfaces.PanelProductionOrderWebRef;

namespace Interfaces
{
    public interface IPanelsProductionOrder
    {
        int CreateFullProductionOrderPan(ProductionOrder prodDoc, IData data, string sessionId);
        int UpdateFullProductionOrderPan(ProductionOrder prodDoc, IData data, string sessionId);
        ProductionOrder GrabPOPan(string sessionId, long docEntry, IData data);

        bool UpdatePanPoStatus(IData data, List<int> docEntryList, bool close, string sessionId);
    }
}