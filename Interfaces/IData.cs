﻿using System;
using System.Collections.Generic;

namespace Interfaces
{
    public interface IData
    {
        int GetDocEntryForPo(int jobNo);
        int GetDocEntryForSo(int jobNo);
        bool UpdateStage(int jobNo, string stageId);
        void InsertErrorToLog(string errorText, string type, int documentNo);
        List<PoInfo> GetListOfPOs(List<int> jobNos);
        JobStatus GetJobStatus(int jobNo);
        List<KeyValuePair<string, decimal>> GetBOM(string jobNo);
        void UpdateAzurd1(int jobNo, int revNo);
        void UpdateRockit(int jobNo, int revNo);
        List<int> GetSOLineNums(int docEntry);
        List<int> GetDelLineNums(int docEntry);
        int GetDelDocNum(int docEntry);
        int GetInvDocNum(int docEntry);
        int GetPODocNum(int docEntry);
        List<ItemRevs> GetItemRevs(int jobNo);
        bool CheckForRevItem(string jobNo, int revNo, string itemRef);
        bool UpdatePOwithCurrentRevision(int docEntry, int crev);
        bool UpdateRDR1withCurrentRevision(int docEntry, int crev);
        bool CheckItemExists(string itemCode);
        int GetSoLineDetail(string itemCode);
        string GetSageSONumber(int delDocNum);

        int GetDocNumForSoWithDocEntry(int docEntry);
        int GetDocEntryForPoIncludingComplete(int jobNo);

        (OrderType,DateTime) WhatIsThisOrder(string jobNo);
        string GetSagePOnumberFromPortal(string jobNo);
        bool IsItemAssemblyBOM(string stockCode);
        List<(string, decimal?)> AssemblyBom(string itemCode);

        bool isDirect(int depNum, int supplierNum, DateTime? reqDate);

        (bool delivered, int delnum, int docentry) IsDelivered(int soDocEntry);
        (bool invoiced, int invnum, int docentry) IsInvoiced(int delDocEntry);

        List<(short itmGrpCod, decimal waste)> GetWastage();
        int GetItmGrpCod(string itemCode);

        bool IsItemActive(string itemCode);

        List<int> GetAllDocEntryForPo(int jobNo);
        List<string> GetAnyBomInactiveLines(int docEntry);
    }

    public class PoInfo
    {
        public int DocEntry { get; set; }
        public string Status { get; set; }
        public string U_AZU_JOB { get; set; }
        public short? U_AZU_RNO { get; set; }
        public int GoodsRecpt { get; set; }
    }

    public class JobStatus
    {
        public int CurrentRevNo { get; set; }
        public int NewRevNo { get; set; }
        public string PoStatus { get; set; }
        public int PoDocEntry { get; set; }
        public int SoDocEntry { get; set; }
        public int JobSource { get; set; }
        public string CardCode { get; set; }
        public int SalesRevNo { get; set; }
        public int AzuRno { get; set; }
    }

    public class ItemRevs
    {
        public string ItemCode { get; set; }
        public int RevNo { get; set; }
    }

    public enum OrderType
    {
       Window = 1, RockdoorPortal = 2, Rockit = 3, RockdoorBm = 4, Panel = 5 
    }
}
