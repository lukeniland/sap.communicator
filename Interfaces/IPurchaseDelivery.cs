﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Interfaces
{
    public interface IPurchaseDelivery
    {
        bool PurchaseGoodsReceipt(List<EntryToBookIn> jobsToBookIn, string sessionId);
        int PurchaseSkinsDelivery(int docEntry, List<BomItem> skinItems, string sessionId, IData data);
    }
}
