﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Interfaces
{
    public interface ISalesInvoices
    {
        int CreateInvoiceNote(int delDocEntry, string sessionId, IData data);
    }
}
