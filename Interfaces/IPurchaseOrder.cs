﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Interfaces
{
    public interface IPurchaseOrder
    {
        int CreatePurchaseOrder(string acctNo, string comment, DateTime dueDate, List<poItem> poItems, string sessionId, IData data, string intRef = "");
        int CreatePurchaseOrderSkins(string acctNo, List<BomItem> skinItems, string sessionId, IData data, string comment);
    }

    public class poItem
    {
        public string ItemCode { get; set; }
        public double Price { get; set; }
    }
}
