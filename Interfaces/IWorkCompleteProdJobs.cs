﻿using System.Collections.Generic;
using Interfaces.ProductionOrderWebRef;

namespace Interfaces
{
    public interface IWorkCompleteProdJobs
    {
        bool CompleteProductionOrder(int salesOrderId, string sessionId, IData data);
        void AddLineToPo(IData data, int jobNo, List<BomItem> itemsToAdd, string sessionId);
        bool UpdatePoStatus(IData data, List<int> docEntryList, bool close, string sessionId);
        bool RemoveLinesFromPo(IData data, int jobNo, List<BomItem> itemsToRemove, string sessionId);
        int CreateRevProdOrder(IData data, long docEntry, string jobNo, int newRevNo, string revItem, string sessionId);
        bool UpdateRevoPOSAP(IData data, long docEntry, string jobNo, int newRevNo, string revItem, string sessionId);
        void UpdateLinesOnPo(IData data, int docEntry, List<BomItem> newLines, string sessionId);
        void UpdateLineQtyOnPo(IData data, int docEntry, string itemCode, double newQuantity, string sessionId);
        bool CancelPo(int docEntry, string sessionId, IData data);
        int CreateFullProductionOrder(ProductionOrder prodDoc, IData data, string sessionId);
        int UpdateFullProductionOrder(ProductionOrder prodDoc, IData data, string sessionId);
        ProductionOrder GrabPO(string sessionId, long docEntry, IData data);

        void InactiveItemCheck(List<int> jobNos, IData data, string sessionId);
    }

    public class BomItem
    {
        public string ItemCode { get; set; }
        public double Qty { get; set; }
    }

    public class EntryToBookIn
    {
        public int DocEntry { get; set; }
        public int LineNo { get; set; }
    }

}
