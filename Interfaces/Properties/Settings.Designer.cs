﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Interfaces.Properties {
    
    
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.VisualStudio.Editors.SettingsDesigner.SettingsSingleFileGenerator", "15.9.0.0")]
    internal sealed partial class Settings : global::System.Configuration.ApplicationSettingsBase {
        
        private static Settings defaultInstance = ((Settings)(global::System.Configuration.ApplicationSettingsBase.Synchronized(new Settings())));
        
        public static Settings Default {
            get {
                return defaultInstance;
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.SpecialSettingAttribute(global::System.Configuration.SpecialSetting.WebServiceUrl)]
        [global::System.Configuration.DefaultSettingValueAttribute("http://192.168.177.195/B1WS/Service.asmx")]
        public string Interfaces_ProductionOrderWebRef_ProductionOrdersService {
            get {
                return ((string)(this["Interfaces_ProductionOrderWebRef_ProductionOrdersService"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.SpecialSettingAttribute(global::System.Configuration.SpecialSetting.WebServiceUrl)]
        [global::System.Configuration.DefaultSettingValueAttribute("http://192.168.177.195/B1WS/Service.asmx")]
        public string Interfaces_ItemsWebRef_ItemsService {
            get {
                return ((string)(this["Interfaces_ItemsWebRef_ItemsService"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.SpecialSettingAttribute(global::System.Configuration.SpecialSetting.WebServiceUrl)]
        [global::System.Configuration.DefaultSettingValueAttribute("http://192.168.177.195/B1WS/Service.asmx")]
        public string Interfaces_PurchaseOrderWebRef_PurchaseOrdersService {
            get {
                return ((string)(this["Interfaces_PurchaseOrderWebRef_PurchaseOrdersService"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.SpecialSettingAttribute(global::System.Configuration.SpecialSetting.WebServiceUrl)]
        [global::System.Configuration.DefaultSettingValueAttribute("http://192.168.177.195/B1WS/Service.asmx")]
        public string Interfaces_GenEntryWebRef_InventoryGenEntryService {
            get {
                return ((string)(this["Interfaces_GenEntryWebRef_InventoryGenEntryService"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.SpecialSettingAttribute(global::System.Configuration.SpecialSetting.WebServiceUrl)]
        [global::System.Configuration.DefaultSettingValueAttribute("http://192.168.177.195/B1WS/Service.asmx")]
        public string Interfaces_PurchaseDelNoteRef_PurchaseDeliveryNotesService {
            get {
                return ((string)(this["Interfaces_PurchaseDelNoteRef_PurchaseDeliveryNotesService"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.SpecialSettingAttribute(global::System.Configuration.SpecialSetting.WebServiceUrl)]
        [global::System.Configuration.DefaultSettingValueAttribute("http://192.168.177.195/B1WS/Service.asmx")]
        public string Interfaces_PurchaseInvoiceWebRef_PurchaseInvoicesService {
            get {
                return ((string)(this["Interfaces_PurchaseInvoiceWebRef_PurchaseInvoicesService"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.SpecialSettingAttribute(global::System.Configuration.SpecialSetting.WebServiceUrl)]
        [global::System.Configuration.DefaultSettingValueAttribute("http://192.168.177.195/B1WS/Service.asmx")]
        public string Interfaces_LoginWebRef_LoginService {
            get {
                return ((string)(this["Interfaces_LoginWebRef_LoginService"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.SpecialSettingAttribute(global::System.Configuration.SpecialSetting.WebServiceUrl)]
        [global::System.Configuration.DefaultSettingValueAttribute("http://192.168.177.195/B1WS/Service.asmx")]
        public string Interfaces_PanelProductionOrderWebRef_ProductionOrdersService {
            get {
                return ((string)(this["Interfaces_PanelProductionOrderWebRef_ProductionOrdersService"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.SpecialSettingAttribute(global::System.Configuration.SpecialSetting.WebServiceUrl)]
        [global::System.Configuration.DefaultSettingValueAttribute("http://192.168.177.195/B1WS/Service.asmx")]
        public string Interfaces_PanelsSalesOrderWebRef_OrdersService {
            get {
                return ((string)(this["Interfaces_PanelsSalesOrderWebRef_OrdersService"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.SpecialSettingAttribute(global::System.Configuration.SpecialSetting.WebServiceUrl)]
        [global::System.Configuration.DefaultSettingValueAttribute("http://192.168.177.195/B1WS/Service.asmx")]
        public string Interfaces_DeliveryWebRef_DeliveryNotesService {
            get {
                return ((string)(this["Interfaces_DeliveryWebRef_DeliveryNotesService"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.SpecialSettingAttribute(global::System.Configuration.SpecialSetting.WebServiceUrl)]
        [global::System.Configuration.DefaultSettingValueAttribute("http://192.168.177.195/B1WS/Service.asmx")]
        public string Interfaces_InvoiceWebRef_InvoicesService {
            get {
                return ((string)(this["Interfaces_InvoiceWebRef_InvoicesService"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.SpecialSettingAttribute(global::System.Configuration.SpecialSetting.WebServiceUrl)]
        [global::System.Configuration.DefaultSettingValueAttribute("http://192.168.177.195/B1WS/Service.asmx")]
        public string Interfaces_SalesOrderWebRef_OrdersService {
            get {
                return ((string)(this["Interfaces_SalesOrderWebRef_OrdersService"]));
            }
        }
    }
}
