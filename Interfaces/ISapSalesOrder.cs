﻿using System;
using System.Collections.Generic;
using Interfaces.SalesOrderWebRef;

namespace Interfaces
{
    public interface ISapSalesOrder
    {
        bool RemoveProdLine(int prodJobId, string sessionId, IData data);
        bool UpdateSalesOrder(int salesOrderId, string sessionId, IData data, string jobNo, int revNo);
        bool CancelSo(int docEntry, string sessionId, IData data);
        int CreateSalesOrder(List<BomItem> items, string cardCode, string comment, string sessionId, IData data, string gapAcctCode);
        int CreateFullSalesOrder(Document soDoc, IData data, string sessionId);
        int UpdateFullSalesOrder(Document soDoc, IData data, string sessionId);
        Document GrabSO(string sessionId, long docEntry, IData data);
        bool UpdateDelDate(int jobNo, DateTime newDelDate, string sessionId, IData data);
    }
}
