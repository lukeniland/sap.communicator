﻿namespace Interfaces
{
    public interface IWriteOfJob
    {
        bool SetWriteOfFlag(int jobNo, IData rockdoorData);
        bool WorkCompleteProdOrder(int jobNumber, string sessionId, IWorkCompleteProdJobs workCompProd, IData rockdoorData);
        bool CreateReciptFromProduction(int jobNumber, string sessionId, IReceiptFromProduction prodReceipt, IData rockdoorData);
        bool RemoveProdOrderLine(int jobNmber, string sessionId, ISapSalesOrder sapSalesOrder, IData rockdoorData);
    }
}
