﻿using Interfaces.PanelsSalesOrderWebRef;

namespace Interfaces
{
    public interface IPanelsSalesOrder
    {
        int CreateFullSalesOrder(Document soDoc, IData data, string sessionId);
        int UpdateFullSalesOrder(Document soDoc, IData data, string sessionId);
        Document GrabSOPan(string sessionId, long docEntry, IData data);
    }
}