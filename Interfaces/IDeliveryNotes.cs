﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Interfaces
{
    public interface IDeliveryNotes
    {
        int CreateDeliveryNote(int soDocEntry, string sessionId, IData data);
        int CreateDeliveryNoteReturnDocEntry(int soDocEntry, string sessionId, IData data);
        int CreateDeliveryNoteLineSpecific(int soDocEntry, int lineNum, string sessionId, IData data);
    }
}
