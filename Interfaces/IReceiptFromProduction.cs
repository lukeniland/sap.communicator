﻿using System.Collections.Generic;

namespace Interfaces
{
    public interface IReceiptFromProduction
    {
        bool CreateReceipt(int jobNumber,string sessionId, IData data);
        bool CreateReceiptMultipleLines(List<int> docEntryList, string sessionId, IData data);
    }
}
