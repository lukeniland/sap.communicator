﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Interfaces
{
    public interface IPurchaseInvoice
    {
        int CreatePurchaseInvoice(int recDocEntry, List<BomItem> skinItems, string sessionId, IData data);
    }
}
