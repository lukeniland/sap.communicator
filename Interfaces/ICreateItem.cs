﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Interfaces
{
    public interface ICreateItem
    {
        bool CreateItem(string revItem, int revNo, string jobNo, string cardCode, string sessionId, IData data);
        bool CreateGlassItem(string itemCode, string itemName, double itemCost, double sellingPrice, int grpCode, string sessionId, IData data);
        bool UpdateGlassItem(string itemCode, double itemCost, double sellingPrice, string sessionId, IData data);

        bool CreateNewItem(string itemName, string itemDesc, double costPrice, double sellingPrice, int revision, ItemGroup itmGroup, string sessionId, IData data, int overrideItmGrp = 0);
    }

    public enum ItemGroup { Rockdoor = 100, Window = 152, Other = 0 }
}
