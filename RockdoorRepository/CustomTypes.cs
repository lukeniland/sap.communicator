﻿namespace RockdoorRepository
{
        public enum Database { Rockdoorlive = 1, Rockdoortest = 2, PanelsLive = 3, PanelsTest = 4 }

        

    public class CancelOrder
    {
        public int jobNo { get; set; }
        public int soDocEntry { get; set; }
        public int poDocEntry { get; set; }
        public int soLineNum { get; set; }
        public int soNoOfLines { get; set; }
    }

}
