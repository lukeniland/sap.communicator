﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using Interfaces;
using Serilog;

namespace RockdoorRepository.Repos
{
    public class DataRepo : IDisposable, IData
    {
        public RockdoorEntities Context { get; private set; }

        private Database _sapDatabaseName;


        public DataRepo(Database db)
        {
            _sapDatabaseName = db;
            Context = new RockdoorEntities(db.ToString());
        }

        public bool IsLiveSapDb()
        {
            return _sapDatabaseName.ToString().ToLower().Contains("live");
        }

        public string GetProductsPortalDbName()
        {
            return IsLiveSapDb() ? "Products_Portal" : "Products_Portal_Test";
        }

        public string GetSageSONumber(int delDocNum)
        {
            var query = (from o in Context.ODLNs
                join d in Context.DLN1 on o.DocEntry equals d.DocEntry
                join dl in Context.RDR1 on d.BaseEntry equals dl.DocEntry
                join ol in Context.ORDRs on dl.DocEntry equals ol.DocEntry
                where o.DocNum == delDocNum
                select new {ol.U_RDR_SageSO}).FirstOrDefault();

            return query.U_RDR_SageSO;
        }

        public int GetDocEntryForPo(int jobNo)
        {
            var query = from o in Context.OWORs
                where o.U_AZU_JOB == jobNo.ToString() && o.U_AZU_RNO == 0 && o.Status != "C" && o.Status != "L"
                select o.DocEntry;
            
            if (query.Any())
            {
                return Convert.ToInt32(query.FirstOrDefault());
            }
            return 0;
        }
        public List<int> GetAllDocEntryForPo(int jobNo)
        {
            var query = from o in Context.OWORs
                        where o.U_AZU_JOB == jobNo.ToString() && o.Status != "C" && o.Status != "L"
                        select o.DocEntry;

            if (query.Any())
            {
                return query.ToList();
            }
            return new List<int>();
        }

        public List<string> GetAnyBomInactiveLines(int docEntry)
        {
            var query = (from o in Context.WOR1 
                        join i in Context.OITMs on o.ItemCode equals i.ItemCode 
                        where o.DocEntry == docEntry && i.validFor == "N" && i.frozenFor == "Y"
                        select i.ItemCode).ToList();
            return query;
        }

        public int GetDocEntryForPoIncludingComplete(int jobNo)
        {
            var query = from o in Context.OWORs
                where o.U_AZU_JOB == jobNo.ToString() && o.U_AZU_RNO == 0 && o.Status != "C" 
                select o.DocEntry;

            if (query.Any())
            {
                return Convert.ToInt32(query.FirstOrDefault());
            }
            return 0;
        }

        public int GetDocEntryForSo(int jobNo)
        {
            var query = from t0 in Context.RDR1
                          join t1 in Context.ORDRs on t0.DocEntry equals t1.DocEntry
                          where t1.DocStatus != "C" && t0.U_AZU_JOB == jobNo.ToString()
                         select t0.DocEntry;
            if (query.Any())
            {
                return Convert.ToInt32(query.FirstOrDefault());
            }
            return 0;
        }

        public bool IsItemAssemblyBOM(string stockCode)
        {
            var query = (from t0 in Context.OITMs
                where t0.ItemCode == stockCode && t0.InvntItem == "N" && t0.TreeType != "N"
                select t0.TreeType).FirstOrDefault();

            return query != null;
        }

        public List<(string, decimal?)> AssemblyBom(string itemCode)
        {
                return (from t0 in Context.OITTs
                    join t1 in Context.ITT1 on t0.Code equals t1.Father
                    where t0.Code == itemCode
                    select new {t1.Code, t1.Quantity}).AsEnumerable().Select(o => (o.Code, o.Quantity)).ToList();     
        }

        public int GetDocNumForSoWithDocEntry(int docEntry)
        {
            var query = (from o in Context.ORDRs
                where o.DocEntry == docEntry
                select o.DocNum).FirstOrDefault();

            return query;
        }

        //update a stage in sap
        public bool UpdateStage(int jobNo, string stageId)
        {
            var jobNoStr = jobNo.ToString();
            int rowsAffected = Context.sp_BarCode_updateWOstages(jobNoStr, stageId);
            return rowsAffected > 0;
        }

        public void InsertErrorToLog(string errorText, string type, int documentNo)
        {
            SAPCommunicatorError error = new SAPCommunicatorError
            {
                DocumentID = documentNo,
                ErrorText = errorText,
                Timestamp = DateTime.Now,
                Type = type
            };

            Context.SAPCommunicatorErrors.Add(error);
            Context.SaveChanges();
        }

        public (bool delivered, int delnum, int docentry) IsDelivered(int soDocEntry)
        {
            var trgetEntry = (from t0 in Context.RDR1
                join t1 in Context.ORDRs on t0.DocEntry equals t1.DocEntry
                where t0.DocEntry == soDocEntry
                select t0.TrgetEntry).FirstOrDefault();

            if (trgetEntry != null)
            {
                //use trget entry to get delivery docnum
                var delDocNum = Context.ODLNs.FirstOrDefault(x => x.DocEntry == trgetEntry).DocNum;

                if (delDocNum != null)
                    return (true, delDocNum.Value, trgetEntry.Value);
            }
            return (false, 0, 0);
        }

        public (bool invoiced, int invnum, int docentry) IsInvoiced(int delDocEntry)
        {
            var trgetEntry = (from t0 in Context.DLN1
                join t1 in Context.ODLNs on t0.DocEntry equals t1.DocEntry
                where t0.DocEntry == delDocEntry
                select t0.TrgetEntry).FirstOrDefault();

            if (trgetEntry != null)
            {
                //use trget entry to get delivery docnum
                var invDocNum = Context.OINVs.FirstOrDefault(x => x.DocEntry == trgetEntry).DocNum;

                if (invDocNum != null)
                    return (true, invDocNum.Value, trgetEntry.Value);
            }
            return (false, 0, 0);
        }

        //get all po's associated with a job number sorted in Revision no descending order
        public List<PoInfo> GetListOfPOs(List<int> jobNos)
        {
            List<PoInfo> poList = new List<PoInfo>();

            //make sure jobno list is distinct
            var cleanedJobs = jobNos.Distinct();

            foreach (int jobNo in cleanedJobs)
            {
                var dets = Context.sp_BarCode_GetPOsAll(jobNo.ToString()).ToList();

                foreach (var po in dets)
                {
                    PoInfo pi = new PoInfo
                    {
                        DocEntry = po.DocEntry,
                        GoodsRecpt = po.GoodsRecpt,
                        Status = po.Status,
                        U_AZU_JOB = po.U_AZU_JOB,
                        U_AZU_RNO = po.U_AZU_RNO
                    };
                    poList.Add(pi);
                }
            }
            return poList;
        }

        public JobStatus GetJobStatus(int jobNo)
        {
            var pinf = Context.sp_SAPComm_GetPOInfo(jobNo.ToString()).FirstOrDefault();
            return new JobStatus
            {
                CurrentRevNo = Convert.ToInt32(pinf.CurrRev),
                JobSource = pinf.OrderType.Value,
                NewRevNo = 0,
                PoDocEntry = pinf.PoDocEntry,
                SoDocEntry = pinf.SODocentry,
                PoStatus = pinf.Status, 
                CardCode = pinf.CardCode,
                SalesRevNo = Convert.ToInt32(pinf.RDR1Rno), 
                AzuRno = Convert.ToInt32(pinf.AzuRno)
            };
        }

        public List<KeyValuePair<string, decimal>> GetBOM(string jobNo)
        {
           return (from x in Context.C_AZURD2
                    where x.U_AZU_JOB == jobNo && !x.U_AZU_ITEM.StartsWith(jobNo)
                    select x).AsEnumerable()
                .Select(o => new KeyValuePair<string, decimal>(o.U_AZU_ITEM, o.U_AZU_QTY.Value))
                .ToList();
        }

        public List<(short itmGrpCod, decimal waste)> GetWastage()
        {
           return (from x in Context.tblWastages
                    select x).AsEnumerable()
                .Select(o => (itmGrpCod: o.ItmsGrpCod.Value, waste: o.PercentWastage.Value)).ToList();
        }

        public bool IsItemActive(string itemCode)
        {
            var res = (from x in Context.OITMs
                       where x.ItemCode == itemCode && x.validFor == "N" && x.frozenFor == "Y"
                       select x).FirstOrDefault();
            if (res == null)
            {
                return true;
            }
            return false;
        }

        public int GetItmGrpCod(string itemCode)
        {
            var itmsGrpCod = Context.OITMs.FirstOrDefault(x => x.ItemCode == itemCode).ItmsGrpCod;
            return itmsGrpCod ?? 0;
        }

        public void UpdateAzurd1(int jobNo, int revNo)
        {
            C_AZURD1 azu1 = (from x in Context.C_AZURD1
                where x.U_AZU_JOB == jobNo.ToString()
                select x).FirstOrDefault();

            azu1.U_AZU_RNO = Convert.ToInt16(revNo);
            azu1.U_AZU_RTYP = "R";
            
            Context.SaveChanges();
        }

        public void UpdateRockit(int jobNo, int revNo)
        {
            Context.sp_SAPComm_UpdateRockit(jobNo.ToString(), revNo);
        }

        public List<int> GetSOLineNums(int docEntry)
        {
            return (from x in Context.RDR1
                where x.DocEntry == docEntry
                select x.LineNum).ToList();
        }

        public List<int> GetDelLineNums(int docEntry)
        {
            return (from x in Context.DLN1
                    where x.DocEntry == docEntry
                    select x.LineNum).ToList();
        }

        public int GetSoLineDetail(string itemCode)
        {
            return (from x in Context.RDR1 
                    where x.ItemCode == itemCode
                    select x.LineNum).FirstOrDefault();
        }

        public int GetDelDocNum(int docEntry)
        {
            return (from x in Context.ODLNs
                where x.DocEntry == docEntry
                select x.DocNum).FirstOrDefault().Value;
        }

        public int GetInvDocNum(int docEntry)
        {
            return (from x in Context.OINVs
                    where x.DocEntry == docEntry
                    select x.DocNum).FirstOrDefault().Value;
        }

        public int GetPODocNum(int docEntry)
        {
            return (from x in Context.OPORs
                where x.DocEntry == docEntry
                select x.DocNum).FirstOrDefault();
        }

        public List<ItemRevs> GetItemRevs(int jobNo)
        {
            var itmRevs = Context.sp_SAPComm_GetRevItems(jobNo.ToString()).ToList();

            return itmRevs.Select(itm => new ItemRevs
            {
                ItemCode = itm.ItemCode, RevNo = Convert.ToInt32(itm.U_AZU_RNO)
            }).ToList();
        }

        public bool CheckItemExists(string itemCode)
        {
            return Context.OITMs.Any(x => x.ItemCode == itemCode);
        }

        public bool CheckForRevItem(string jobNo, int revNo, string itemRef)
        {
            return (from x in Context.OWORs 
                    join w in Context.WOR1 on x.DocEntry equals w.DocEntry 
                where x.U_AZU_RNO == revNo && x.U_AZU_JOB == jobNo && x.Status != "C" && w.ItemCode == itemRef
                select x).Any();
        }

        public (OrderType, DateTime) WhatIsThisOrder(string jobNo)
        {
            var tp = (from x in Context.ORDRs
                      join w in Context.RDR1 on x.DocEntry equals w.DocEntry
                      where w.U_AZU_JOB == jobNo
                      select new { x.U_Window, x.U_RDR_PRIO, x.DocDueDate }).FirstOrDefault();

            if (tp == null) return (OrderType.RockdoorPortal, DateTime.Today);
            if (tp.U_Window == 1)
            {
                return (OrderType.Window, tp.DocDueDate.Value);
            }

            if (tp.U_Window == 0 && tp.U_RDR_PRIO == 1)
            {
                return (OrderType.RockdoorBm, tp.DocDueDate.Value);
            }

            if (tp.U_Window == 0 && tp.U_RDR_PRIO == 2)
            {
                return (OrderType.RockdoorPortal, tp.DocDueDate.Value);
            }

            if (tp.U_Window == 0 && tp.U_RDR_PRIO == 0)
            {
                return (OrderType.Rockit, tp.DocDueDate.Value);
            }
            return (OrderType.RockdoorPortal, DateTime.Today);

        }

        public string GetSagePOnumberFromPortal(string jobNo)
        {
            var productsPortalDbName = GetProductsPortalDbName();

            var sql = "SELECT PO_number_sage " +
                    $"FROM {productsPortalDbName}.dbo.Order_ProductItem opi " +
                    $"INNER join {productsPortalDbName}.dbo.Order_Products op ON opi.SalesOrderID = op.SalesOrderID AND opi.FrameNo = op.FrameNo " +
                    "WHERE SapProductionNumber = @jobNo AND po_number_sage IS NOT NULL";

            var result = Context.Database.SqlQuery<string>(sql, new SqlParameter("@jobNo", jobNo))
                .FirstOrDefault();

            return result ?? "";
        }

        public bool isDirect(int depNum, int supplierNum, DateTime? reqDate)
        {
            Log.Information($"IsDirect - depNum {depNum} - supplierNum - {supplierNum} - reqDate {reqDate.Value}");
            var productsPortalDbName = GetProductsPortalDbName();
            var ruleId = 0;
            var dayOfWeek = (int)DayOfWeek.Monday;

            if (reqDate != null)
            {
                dayOfWeek = (int)reqDate.Value.DayOfWeek;
            }

            int loopBreak = 0;

            while (ruleId == 0 && loopBreak != 2)
            {
                ruleId = GetRuleIdForSupplier(productsPortalDbName, depNum, supplierNum, dayOfWeek);
                //rule not found add +1
                if (dayOfWeek < 6)
                {
                    dayOfWeek++;
                }
                else
                {
                    dayOfWeek = 1;
                    loopBreak++;
                }
            }
            Log.Information($"IsDirect - Rule found {ruleId}");
            if (ruleId == 0)
                return false;

            //check for direct
            var direct = "SELECT via_central " +
                          $"FROM {productsPortalDbName}.[dbo].[Windows_Dates_Rules] " +
                          $"WHERE rule_id = {ruleId} ";
            var ruleval = Context.Database.SqlQuery<int>(direct).FirstOrDefault();

            return ruleval == 0;

        }

        private int GetRuleIdForSupplier(string ppDb, int depNum, int suppNum, int dayOfweek)
        {
            var day = Enum.GetName(typeof(DayOfWeek), dayOfweek).ToLower();
            
            var sql = $"SELECT {day} " +
                      $"FROM {ppDb}.[dbo].[Windows_Dates_Supplier_Days] " +
                      $"WHERE supplier = {suppNum} AND depot = {depNum}";

            return Context.Database.SqlQuery<int>(sql).FirstOrDefault();
        }

        public bool UpdatePOwithCurrentRevision(int docEntry, int crev)
        {
            var poDoc = Context.OWORs.FirstOrDefault(x => x.DocEntry == docEntry);

            poDoc.U_AZU_CREV = (short?) crev;

            Context.SaveChanges();

            return true;
        }

        public bool UpdateRDR1withCurrentRevision(int docEntry, int crev)
        {
            var poDoc = Context.RDR1.FirstOrDefault(x => x.DocEntry == docEntry);

            poDoc.U_AZU_RNO = (short?)crev;
            poDoc.U_AZU_RTYP = "R";

            Context.SaveChanges();

            return true;
        }

        public void Dispose()
        {
            Context.Dispose();
        }
    }
}
