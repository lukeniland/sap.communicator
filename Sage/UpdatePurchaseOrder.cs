﻿using System;
using ErpCommunicator;
using ErpCommunicator.Purchasing;

namespace Sage
{
    public class UpdatePurchaseOrder
    {
        private readonly IUpdatePurchaseOrders _poUpdate;
        private readonly IGetPurchaseOrders _poGet;

        public UpdatePurchaseOrder()
        {
            var fac = ErpFactory.Build();
            _poUpdate = fac.Instance<IUpdatePurchaseOrders>();
            _poGet = fac.Instance<IGetPurchaseOrders>();
        }


        public string UpdateErpPurchaseOrder(string poNum, DateTime dateToUpdate)
        {
            var po = _poGet.GetPo(poNum);

            if (po.Success)
            {
                if (po.Result.Lines[0].ExpectedDate != dateToUpdate)
                {

                    foreach (var line in po.Result.Lines)
                    {
                        line.ExpectedDate = dateToUpdate;
                    }

                    var res = _poUpdate.UpdateErpPurchaseOrder(po.Result);

                    return res.Success ? res.Result.PurchaseOrderNumber : res.Message;
                }
                else
                {
                    return "date update not required";
                }
            }

            return "po not found";
        }
    }
}
