﻿namespace SAPWStester
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnAddlinestoPO = new System.Windows.Forms.Button();
            this.txtJobNo = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.btnSesssion = new System.Windows.Forms.Button();
            this.lblSessionid = new System.Windows.Forms.Label();
            this.btnLogout = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.btnWorkComplete = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.btnCreateDel = new System.Windows.Forms.Button();
            this.txtLineNum = new System.Windows.Forms.TextBox();
            this.btnDeliveryLine = new System.Windows.Forms.Button();
            this.txtResponse = new System.Windows.Forms.TextBox();
            this.btnSkinMovement = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnNewItem = new System.Windows.Forms.Button();
            this.btnHasSage = new System.Windows.Forms.Button();
            this.TxtHasSage = new System.Windows.Forms.TextBox();
            this.btnGetSO = new System.Windows.Forms.Button();
            this.CmbSapDatabase = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.TxtSalesOrderDocEntry = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.btnraiseTestPO = new System.Windows.Forms.Button();
            this.btnRaisePO = new System.Windows.Forms.Button();
            this.IsDirect = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnAddlinestoPO
            // 
            this.btnAddlinestoPO.Location = new System.Drawing.Point(8, 222);
            this.btnAddlinestoPO.Name = "btnAddlinestoPO";
            this.btnAddlinestoPO.Size = new System.Drawing.Size(132, 23);
            this.btnAddlinestoPO.TabIndex = 2;
            this.btnAddlinestoPO.Text = "Add lines to PO";
            this.btnAddlinestoPO.UseVisualStyleBackColor = true;
            this.btnAddlinestoPO.Click += new System.EventHandler(this.btnAddlinestoPO_Click);
            // 
            // txtJobNo
            // 
            this.txtJobNo.Location = new System.Drawing.Point(60, 131);
            this.txtJobNo.Name = "txtJobNo";
            this.txtJobNo.Size = new System.Drawing.Size(80, 20);
            this.txtJobNo.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 134);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(41, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Job No";
            // 
            // btnSesssion
            // 
            this.btnSesssion.Location = new System.Drawing.Point(8, 56);
            this.btnSesssion.Name = "btnSesssion";
            this.btnSesssion.Size = new System.Drawing.Size(75, 23);
            this.btnSesssion.TabIndex = 5;
            this.btnSesssion.Text = "Get Session Id";
            this.btnSesssion.UseVisualStyleBackColor = true;
            this.btnSesssion.Click += new System.EventHandler(this.btnSesssion_Click);
            // 
            // lblSessionid
            // 
            this.lblSessionid.AutoSize = true;
            this.lblSessionid.Location = new System.Drawing.Point(89, 61);
            this.lblSessionid.Name = "lblSessionid";
            this.lblSessionid.Size = new System.Drawing.Size(51, 13);
            this.lblSessionid.TabIndex = 6;
            this.lblSessionid.Text = "sessionId";
            // 
            // btnLogout
            // 
            this.btnLogout.Location = new System.Drawing.Point(8, 85);
            this.btnLogout.Name = "btnLogout";
            this.btnLogout.Size = new System.Drawing.Size(75, 23);
            this.btnLogout.TabIndex = 7;
            this.btnLogout.Text = "Logout";
            this.btnLogout.UseVisualStyleBackColor = true;
            this.btnLogout.Click += new System.EventHandler(this.btnLogout_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(8, 251);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(132, 23);
            this.button1.TabIndex = 8;
            this.button1.Text = "Remove Lines from PO";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(8, 280);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(132, 23);
            this.button2.TabIndex = 9;
            this.button2.Text = "Write Off Job";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // btnWorkComplete
            // 
            this.btnWorkComplete.Location = new System.Drawing.Point(8, 309);
            this.btnWorkComplete.Name = "btnWorkComplete";
            this.btnWorkComplete.Size = new System.Drawing.Size(132, 23);
            this.btnWorkComplete.TabIndex = 10;
            this.btnWorkComplete.Text = "Work complete";
            this.btnWorkComplete.UseVisualStyleBackColor = true;
            this.btnWorkComplete.Click += new System.EventHandler(this.btnWorkComplete_Click);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(191, 251);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(113, 23);
            this.button3.TabIndex = 11;
            this.button3.Text = "Raise remake";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // btnCreateDel
            // 
            this.btnCreateDel.Location = new System.Drawing.Point(8, 164);
            this.btnCreateDel.Name = "btnCreateDel";
            this.btnCreateDel.Size = new System.Drawing.Size(132, 23);
            this.btnCreateDel.TabIndex = 12;
            this.btnCreateDel.Text = "Create delivery";
            this.btnCreateDel.UseVisualStyleBackColor = true;
            this.btnCreateDel.Click += new System.EventHandler(this.btnCreateDel_Click);
            // 
            // txtLineNum
            // 
            this.txtLineNum.Location = new System.Drawing.Point(203, 195);
            this.txtLineNum.Name = "txtLineNum";
            this.txtLineNum.Size = new System.Drawing.Size(72, 20);
            this.txtLineNum.TabIndex = 13;
            // 
            // btnDeliveryLine
            // 
            this.btnDeliveryLine.Location = new System.Drawing.Point(8, 193);
            this.btnDeliveryLine.Name = "btnDeliveryLine";
            this.btnDeliveryLine.Size = new System.Drawing.Size(132, 23);
            this.btnDeliveryLine.TabIndex = 14;
            this.btnDeliveryLine.Text = "Create delivery line";
            this.btnDeliveryLine.UseVisualStyleBackColor = true;
            this.btnDeliveryLine.Click += new System.EventHandler(this.btnDeliveryLine_Click);
            // 
            // txtResponse
            // 
            this.txtResponse.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtResponse.Location = new System.Drawing.Point(8, 438);
            this.txtResponse.Multiline = true;
            this.txtResponse.Name = "txtResponse";
            this.txtResponse.Size = new System.Drawing.Size(329, 87);
            this.txtResponse.TabIndex = 15;
            // 
            // btnSkinMovement
            // 
            this.btnSkinMovement.Location = new System.Drawing.Point(191, 280);
            this.btnSkinMovement.Name = "btnSkinMovement";
            this.btnSkinMovement.Size = new System.Drawing.Size(113, 23);
            this.btnSkinMovement.TabIndex = 16;
            this.btnSkinMovement.Text = "test skin movement";
            this.btnSkinMovement.UseVisualStyleBackColor = true;
            this.btnSkinMovement.Click += new System.EventHandler(this.btnSkinMovement_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(191, 309);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(113, 23);
            this.btnCancel.TabIndex = 17;
            this.btnCancel.Text = "Test Cancellation";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnNewItem
            // 
            this.btnNewItem.Location = new System.Drawing.Point(8, 338);
            this.btnNewItem.Name = "btnNewItem";
            this.btnNewItem.Size = new System.Drawing.Size(132, 23);
            this.btnNewItem.TabIndex = 18;
            this.btnNewItem.Text = "Create New Job Item";
            this.btnNewItem.UseVisualStyleBackColor = true;
            this.btnNewItem.Click += new System.EventHandler(this.btnNewItem_Click);
            // 
            // btnHasSage
            // 
            this.btnHasSage.Location = new System.Drawing.Point(8, 367);
            this.btnHasSage.Name = "btnHasSage";
            this.btnHasSage.Size = new System.Drawing.Size(132, 23);
            this.btnHasSage.TabIndex = 19;
            this.btnHasSage.Text = "has sage SO?";
            this.btnHasSage.UseVisualStyleBackColor = true;
            this.btnHasSage.Click += new System.EventHandler(this.btnHasSage_Click);
            // 
            // TxtHasSage
            // 
            this.TxtHasSage.Location = new System.Drawing.Point(146, 369);
            this.TxtHasSage.Name = "TxtHasSage";
            this.TxtHasSage.Size = new System.Drawing.Size(71, 20);
            this.TxtHasSage.TabIndex = 20;
            // 
            // btnGetSO
            // 
            this.btnGetSO.Location = new System.Drawing.Point(8, 396);
            this.btnGetSO.Name = "btnGetSO";
            this.btnGetSO.Size = new System.Drawing.Size(132, 23);
            this.btnGetSO.TabIndex = 21;
            this.btnGetSO.Text = "Get SO";
            this.btnGetSO.UseVisualStyleBackColor = true;
            this.btnGetSO.Click += new System.EventHandler(this.btnGetSO_Click);
            // 
            // CmbSapDatabase
            // 
            this.CmbSapDatabase.FormattingEnabled = true;
            this.CmbSapDatabase.Location = new System.Drawing.Point(83, 19);
            this.CmbSapDatabase.Name = "CmbSapDatabase";
            this.CmbSapDatabase.Size = new System.Drawing.Size(121, 21);
            this.CmbSapDatabase.TabIndex = 22;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(5, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(77, 13);
            this.label1.TabIndex = 23;
            this.label1.Text = "SAP Database";
            // 
            // TxtSalesOrderDocEntry
            // 
            this.TxtSalesOrderDocEntry.Location = new System.Drawing.Point(203, 398);
            this.TxtSalesOrderDocEntry.Name = "TxtSalesOrderDocEntry";
            this.TxtSalesOrderDocEntry.Size = new System.Drawing.Size(72, 20);
            this.TxtSalesOrderDocEntry.TabIndex = 24;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(156, 198);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(41, 13);
            this.label3.TabIndex = 25;
            this.label3.Text = "LineNo";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(146, 401);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(51, 13);
            this.label4.TabIndex = 26;
            this.label4.Text = "DocEntry";
            // 
            // btnraiseTestPO
            // 
            this.btnraiseTestPO.Location = new System.Drawing.Point(191, 339);
            this.btnraiseTestPO.Name = "btnraiseTestPO";
            this.btnraiseTestPO.Size = new System.Drawing.Size(113, 23);
            this.btnraiseTestPO.TabIndex = 27;
            this.btnraiseTestPO.Text = "raiseTestPO";
            this.btnraiseTestPO.UseVisualStyleBackColor = true;
            this.btnraiseTestPO.Click += new System.EventHandler(this.btnraiseTestPO_Click);
            // 
            // btnRaisePO
            // 
            this.btnRaisePO.Location = new System.Drawing.Point(191, 221);
            this.btnRaisePO.Name = "btnRaisePO";
            this.btnRaisePO.Size = new System.Drawing.Size(113, 23);
            this.btnRaisePO.TabIndex = 28;
            this.btnRaisePO.Text = "Raise PO";
            this.btnRaisePO.UseVisualStyleBackColor = true;
            this.btnRaisePO.Click += new System.EventHandler(this.btnRaisePO_Click);
            // 
            // IsDirect
            // 
            this.IsDirect.Location = new System.Drawing.Point(203, 164);
            this.IsDirect.Name = "IsDirect";
            this.IsDirect.Size = new System.Drawing.Size(75, 23);
            this.IsDirect.TabIndex = 29;
            this.IsDirect.Text = "IsDirect";
            this.IsDirect.UseVisualStyleBackColor = true;
            this.IsDirect.Click += new System.EventHandler(this.IsDirect_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(349, 537);
            this.Controls.Add(this.IsDirect);
            this.Controls.Add(this.btnRaisePO);
            this.Controls.Add(this.btnraiseTestPO);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.TxtSalesOrderDocEntry);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.CmbSapDatabase);
            this.Controls.Add(this.btnGetSO);
            this.Controls.Add(this.TxtHasSage);
            this.Controls.Add(this.btnHasSage);
            this.Controls.Add(this.btnNewItem);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnSkinMovement);
            this.Controls.Add(this.txtResponse);
            this.Controls.Add(this.btnDeliveryLine);
            this.Controls.Add(this.txtLineNum);
            this.Controls.Add(this.btnCreateDel);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.btnWorkComplete);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.btnLogout);
            this.Controls.Add(this.lblSessionid);
            this.Controls.Add(this.btnSesssion);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtJobNo);
            this.Controls.Add(this.btnAddlinestoPO);
            this.Name = "Form1";
            this.Text = "Form1";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnAddlinestoPO;
        private System.Windows.Forms.TextBox txtJobNo;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnSesssion;
        private System.Windows.Forms.Label lblSessionid;
        private System.Windows.Forms.Button btnLogout;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button btnWorkComplete;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button btnCreateDel;
        private System.Windows.Forms.TextBox txtLineNum;
        private System.Windows.Forms.Button btnDeliveryLine;
        private System.Windows.Forms.TextBox txtResponse;
        private System.Windows.Forms.Button btnSkinMovement;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnNewItem;
        private System.Windows.Forms.Button btnHasSage;
        private System.Windows.Forms.TextBox TxtHasSage;
        private System.Windows.Forms.Button btnGetSO;
        private System.Windows.Forms.ComboBox CmbSapDatabase;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox TxtSalesOrderDocEntry;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button btnraiseTestPO;
        private System.Windows.Forms.Button btnRaisePO;
        private System.Windows.Forms.Button IsDirect;
    }
}

