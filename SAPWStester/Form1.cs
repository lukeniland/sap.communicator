﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Windows.Forms;
using SAPWStester.ServiceReference;

namespace SAPWStester
{
    public partial class Form1 : Form
    {
        private string _sessionId { get; set; }

        private Database _sapCompanyDatabase => ((KeyValuePair<Database, string>)CmbSapDatabase.SelectedItem).Key;

        private string _sapCompanyPassword => IsLiveSapCompany() ? "M4tild4" : "test";

        private int _jobNo => Convert.ToInt32(txtJobNo.Text);

        private string _nl = Environment.NewLine;



        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            PopulateSapDatabaseList();
            TxtSalesOrderDocEntry.Text = "336044";
        }

        private void PopulateSapDatabaseList()
        {
            var list = new List<KeyValuePair<Database, string>>
            {
                new KeyValuePair<Database, string>(Database.Rockdoortest, "Rockdoor TEST"),
                new KeyValuePair<Database, string>(Database.Rockdoorlive, "Rockdoor LIVE"),
                new KeyValuePair<Database, string>(Database.PanelsTest, "Panels TEST"),
                new KeyValuePair<Database, string>(Database.PanelsLive, "Panels LIVE")
            };

            CmbSapDatabase.ValueMember = "Key";
            CmbSapDatabase.DisplayMember = "Value";
            CmbSapDatabase.DataSource = list;
            CmbSapDatabase.SelectedIndex = 0;
        }


        private bool IsLiveSapCompany()
        {
            return ((KeyValuePair<Database, string>)CmbSapDatabase.SelectedItem).Value.Contains("LIVE");
        }


        private void btnSesssion_Click(object sender, EventArgs e)
        {
            var client = new SapCommunicatorClient();

            try
            {
                var sessionId = client.LoginForSessionId(_sapCompanyDatabase, _sapCompanyPassword);
                SetSessionId(sessionId);
            }
            catch (Exception ex)
            {
                MessageBox.Show($"Unable to get SAP session{_nl}{_nl}{ex.Message}", "SAP Session Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnLogout_Click(object sender, EventArgs e)
        {
            var client = new SapCommunicatorClient();
            client.Logout(_sessionId);
            SetSessionId("");
        }

        private void SetSessionId(string sessionId)
        {
            _sessionId = sessionId;
            lblSessionid.Text = sessionId;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var removeItems = GetTestForRemovePo();
            var client = new SapCommunicatorClient();
            client.RemoveStockFromProductionOrderAsync(_sapCompanyDatabase, _jobNo, removeItems.ToArray(), _sessionId);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            var client = new SapCommunicatorClient();
            client.WriteOfProductionOrderAsync(_sapCompanyDatabase, _jobNo, _sessionId);
        }

        private List<BomItem> GetTestForRemovePo()
        {
            List<BomItem> items = new List<BomItem>();
            items.Add(new BomItem
            {
                ItemCode = "W113WH",
                Qty = 1
            });
            items.Add(new BomItem
            {
                ItemCode = "WB07",
                Qty = 1
            });

            return items;
        }

        private void btnAddlinestoPO_Click(object sender, EventArgs e)
        {
            BomItem[] items = new BomItem[2];

            items[0] = new BomItem
            {
                ItemCode = "R1001C",
                Qty = 15
            };
            items[1] = new BomItem
            {
                ItemCode = "R1001B",
                Qty = 10
            };

            var client = new SapCommunicatorClient();
            client.AddLinesToProductionOrderAsync(_sapCompanyDatabase, _jobNo, items, _sessionId);
        }

        private void btnWorkComplete_Click(object sender, EventArgs e)
        {
            int[] jobs;

            if (!string.IsNullOrEmpty(txtJobNo.Text))
            {
                jobs = new int[] { Convert.ToInt32(txtJobNo.Text) };
            }
            else
            {
                jobs = new int[1];

                jobs[0] = 920376; //released
                //jobs[1] = 908396; //planned
                //jobs[2] = 908395; //released receipted
                //jobs[3] = 908389; //revision
            }


            var client = new SapCommunicatorClient();
            client.WorkCompletion(_sapCompanyDatabase, jobs, _sessionId);
        }

        private void button3_Click(object sender, EventArgs e)
        {
            var client = new SapCommunicatorClient();
            BomItem itm = new BomItem
            {
                ItemCode = "WIN794804F1G1", Qty = 1
            };
            List<BomItem> bms = new List<BomItem>();
            bms.Add(itm);
            client.AddLinesToProductionOrder(Database.Rockdoortest, 912406, bms.ToArray(), lblSessionid.Text);
        }


        // Alex - Figured it might be best to ensure any previously created session is closed before exiting
        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            var sessionId = lblSessionid.Text;

            if (!string.IsNullOrEmpty(sessionId) && sessionId != "sessionId")
            {
                var client = new SapCommunicatorClient();
                client.Logout(sessionId);
            }
        }

        private void btnCreateDel_Click(object sender, EventArgs e)
        {
            var sessionId = lblSessionid.Text;

            var client = new SapCommunicatorClient();
            client.CreateDeliveryNote(_jobNo, sessionId, _sapCompanyDatabase);

        }

        private void btnDeliveryLine_Click(object sender, EventArgs e)
        {
            var sessionId = lblSessionid.Text;

            var client = new SapCommunicatorClient();
            txtResponse.Text = client.CreateDeliveryNoteLineSpecific(_jobNo, sessionId, Convert.ToInt32(txtLineNum.Text), _sapCompanyDatabase).ToString();
        }

        private void btnSkinMovement_Click(object sender, EventArgs e)
        {
            var client = new SapCommunicatorClient();

            var items = new BomItem[2];

            items[0] = new BomItem
            {
                ItemCode = "RS001",
                Qty = 2
            };

            items[1] = new BomItem
            {
                ItemCode = "RS015",
                Qty = 3
            };

            client.SkinsForPanels(items, "test", "test", false);
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            List<int> lines = new List<int>();

            using (StreamReader r = new StreamReader("c:\\job2cancel\\job2cancel.txt"))
            {
                string line;
                while ((line = r.ReadLine()) != null)
                {
                    // 4
                    // Insert logic here.
                    // ...
                    // The "line" value is a line in the file.
                    // Add it to our List.
                    lines.Add(Convert.ToInt32(line));
                }
            }

            var client = new SapCommunicatorClient();
            client.CancelFromSap(_sapCompanyDatabase, lines.ToArray(), _sessionId);

            //int[] jobs;
            //jobs = new int[4];

            //jobs[0] = 514931; //released
            //jobs[1] = 514945;
            //jobs[2] = 514946;
            //jobs[3] = 514947;

            //var client = new SapCommunicatorClient();
            //client.CancelFromSap(Database.Rockdoortest, jobs, sessionId);
        }

        private void btnNewItem_Click(object sender, EventArgs e)
        {
            var r = new Random();
            int num = r.Next(700000, 700100);
            var client = new SapCommunicatorClient();
            client.CreateItem(num.ToString(), num.ToString(), 1, 2, 0, ItemGroup.Rockdoor, _sessionId, _sapCompanyDatabase, 0);
        }

        private void btnHasSage_Click(object sender, EventArgs e)
        {
            var client = new SapCommunicatorClient();
            txtResponse.Text = client.HasSageSOrder(_sapCompanyDatabase, Convert.ToInt32(TxtHasSage.Text)).ToString();
        }

        private void btnGetSO_Click(object sender, EventArgs e)
        {
            var client = new SapCommunicatorClient();
            ClearResponse();
            var sessionId = lblSessionid.Text;
            string response;
            var docEntry = Convert.ToInt32(TxtSalesOrderDocEntry.Text);
            var doc = client.FetchSODocument(_sapCompanyDatabase, docEntry, sessionId);

            if (doc == null)
            {
                response = "Not found";
            }
            else
            {
                response = $"DocEntry:{doc.docEntryField}" +
                    $"{_nl}DocNum:{doc.docNumField}" +
                    $"{_nl}Created:{doc.creationDateField:yyyy-MM-dd}" +
                    $"{_nl}Due:{doc.docDueDateField:yyyy-MM-dd}";
            }

            txtResponse.Text = response;
        }


        private void ClearResponse()
        {
            txtResponse.Text = "";
            txtResponse.Refresh();
        }

        private void btnraiseTestPO_Click(object sender, EventArgs e)
        {
            var client = new SapCommunicatorClient();
            var sessionId = lblSessionid.Text;

            ProductionOrder myPOrder = new ProductionOrder();

            myPOrder.customerCodeField = "G012";
            myPOrder.productionOrderTypeField = ProductionOrderProductionOrderType.bopotSpecial;
            myPOrder.productionOrderTypeFieldSpecified = true;
            myPOrder.productionOrderStatusField = ProductionOrderProductionOrderStatus.boposPlanned;
            myPOrder.productionOrderStatusFieldSpecified = true;


                myPOrder.itemNoField = "912269";
                myPOrder.u_AZU_JOBField = "912269";


            //order dates
            myPOrder.creationDateField = DateTime.Now.Date;
            myPOrder.creationDateFieldSpecified = true;
            myPOrder.dueDateField = DateTime.Now.Date;
            myPOrder.dueDateFieldSpecified = true;

            //Order origins
            myPOrder.productionOrderOriginField = ProductionOrderProductionOrderOrigin.bopooSalesOrder;
            myPOrder.productionOrderOriginFieldSpecified = true;
            //myPOrder.productionOrderOriginEntryField = docEntry;
            //myPOrder.productionOrderOriginEntryFieldSpecified = true;


            //now we are going to create the Bill of Materials for this job so
            myPOrder.productionOrderLinesField = new ProductionOrderProductionOrderLine[3];
            
                ProductionOrderProductionOrderLine docLine1 = new ProductionOrderProductionOrderLine
                {
                    itemNoField = "STYLE3EWWG",
                    baseQuantityField = 1,
                    baseQuantityFieldSpecified = true
                };
                myPOrder.productionOrderLinesField[0] = docLine1;

            ProductionOrderProductionOrderLine docLine2 = new ProductionOrderProductionOrderLine
            {
                itemNoField = "R695Z",
                baseQuantityField = 1,
                baseQuantityFieldSpecified = true
            };
            myPOrder.productionOrderLinesField[1] = docLine2;

            ProductionOrderProductionOrderLine docLine3 = new ProductionOrderProductionOrderLine
            {
                itemNoField = "CSTGRPB000006",
                baseQuantityField = 1,
                baseQuantityFieldSpecified = true
            };
            myPOrder.productionOrderLinesField[2] = docLine3;

            

            var response = client.CreateProdOrder(_sapCompanyDatabase, myPOrder, sessionId);
            txtResponse.Text = response.ToString();
        }

        private void btnRaisePO_Click(object sender, EventArgs e)
        {
            var client = new SapCommunicatorClient();
            ClearResponse();
            var sessionId = lblSessionid.Text;

            poItem newItm = new poItem
            {
                ItemCode = "R100C", Price = 99.99
            };

            List<poItem> poItems = new List<poItem>();
            poItems.Add(newItm);

            client.CreatePurchaseOrder("SK002", "TestPO", DateTime.Today, poItems.ToArray() , sessionId, Database.Rockdoortest);
            client.CreatePurchaseOrderWithIntRef("SK002", "TestPO", DateTime.Today, poItems.ToArray(), sessionId, Database.Rockdoortest, "123456456");
        }

        private void IsDirect_Click(object sender, EventArgs e)
        {
            var client = new SapCommunicatorClient();
            txtResponse.Text =  client.IsDirectToSage(_sapCompanyDatabase, 12, 41, Convert.ToDateTime("2020-08-14")).ToString();
        }
    }
}
